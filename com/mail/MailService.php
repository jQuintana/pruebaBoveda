<?php
/**
 * Servicios del recurso Mail
 */
namespace com\mail;

use MNIComponents\Base\TService;
use Mailgun\Mailgun;
use Exception;


/**
 * Servicios del recurso Mail
 *
 * @author 		Israel Hernandez
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Mail
 * @version 	1.1
 * 
 * @Component(name=MailService)
 * @Singleton 
 */
 class MailService
{
	/** @Resource(name=Mail) */
	protected $mail;
	protected $mailgun;
	protected $logger;
	use TService;

	public function conectarMailgun()
	{
		if(!$this->mailgun instanceof Mailgun){
			$this->logger->info("Conectando con el servicio de Mailgun");		
			$this->mailgun = new Mailgun($this->mail->getKey());
		}
	}

	public function sendMail($to, $subject, $text = "", $html = "", $attachment = array())
	{
		try{
			$this->conectarMailgun();
			$this->logger->info("Enviando correo electronico para: $to, con el asunto: $subject");					
			$this->mailgun->sendMessage(
				$this->mail->getDomain(),
				array(
					'from'    	=> $this->mail->getFrom(),
					'to'      	=> $to,
					'subject'	=> $subject,
					'text'   	=> $text,
					'cc'		=> $this->mail->getCc(),
					'bcc'		=> $this->mail->getBcc(),
					'html'		=> $html
				),
				array(
					'attachment'=> $attachment
				)
			);
		}catch(Exception $e){		
			$this->logger->error($e->getMessage());	
			throw new Exception("No fue posible enviar el correo, favor de intentar nuevamente");
		}
	}

	/**
	 * Obtiene de mailgun un log determinado
	 *
	 * @param string $limit
	 * @retrun void
	 */
	public function obtenLog($limit){
		$this->mailgun->get($this->mail->getDomain()."/log",
							array('limit'	=> $limit,
								  'skip'	=> 0
								)
							);
	}
}
