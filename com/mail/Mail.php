<?php
/**
 * Modelo para el recurso Mail
 */
namespace com\mail;

use MNIComponents\Base\TModel;


/**
 * Modelo para el recurso Mail
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Mail
 * @version 	1.1
 */
class Mail
{
	private $key;
	private $domain;
	private $from;
	private $to;
	private $cc;
	private $bcc;
	private $subject;
	private $text;
	private $html;
	private $attachment;
	use TModel;

	public function setKey($key )
	{
		$this->key = $key;
	}
	
	public function getKey()
	{
		return $this->key;
	}
	
	public function setDomain($domain)
	{
		$this->domain = $domain;
	}
	
	public function getDomain()
	{
		return $this->domain;
	}
	
	public function setFrom($from)
	{
		$this->from = $from;
	}
	
	public function getFrom()
	{
		return $this->from;
	}
	
	public function setTo($to)
	{
		$this->to = $to;
	}
	
	public function getTo()
	{
		return $this->to;
	}
	
	public function setCc($cc)
	{
		$this->cc = $cc;
	}
	
	public function getCc()
	{
		return $this->cc;
	}
	
	public function setBcc($bcc)
	{
		$this->bcc = $bcc;
	}
	
	public function getBcc()
	{
		return $this->bcc;
	}
	
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	public function getSubject()
	{
		return $this->subject;
	}
	
	public function setText($text)
	{
		$this->text = $text;
	}
	
	public function getText()
	{
		return $this->text;
	}
	
	public function setHtml($html)
	{
		$this->html = $html;
	}
	
	public function getHtml()
	{
		return $this->html;
	}
	
	public function setAttachment($attachment)
	{
		$this->attachment = $attachment;
	}
	
	public function getAttachment()
	{
		return $this->attachment;
	}
}
