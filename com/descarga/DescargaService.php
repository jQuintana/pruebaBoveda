<?php
/**
 * Servicios del recurso Descarga
 */
namespace com\descarga;

use MNIComponents\Base\TService;
use ZipArchive;
use Exception;

/**
 * Servicios del recurso Descarga
 * 
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Descarga
 * @version 	1.1
 * 
 * @Component(name=DescargaService)
 * @Singleton
 */
class DescargaService
{
	/** @Resource(name=Descarga) */
	protected $descarga;
	/** @Resource(name=DescargaDao) */
	protected $descargaDao;
	/** @Resource(name=FacturaService) */
	protected $facturaService;
	/** @Resource(name=Factura) */
	protected $factura;
	/** @Resource(name=ObjectStorageService) */
	protected $objectStorageService;
	protected $logger;
	const RUTA_DOWNLOADS = "../downloads/";
	use TService;

	/**
	 * Crea un archivo zip, agrega todos los xml's de las facturas determinadas y envia el archivo para su descarga
	 * @param \com\descarga\Descarga $descarga
	 * @return void
	 */
	public function descargarZip($cfdis)
	{
		$array 		= explode(",", $cfdis);
		$zipArchive = new ZipArchive();
		$filename 	= date("Ymd_His")."_boveda.zip";
		
		if($zipArchive->open(self::RUTA_DOWNLOADS.$filename, ZipArchive::CREATE) !== true)
			throw new \Exception("No se pudo crear el archivo");
		
		foreach($array as $key){
			$this->descarga->setIdFactura($key);
			$list = $this->consultar($this->descarga);
			foreach($list as $key => $val){
				$this->factura->__fromArray($val);
				$fileName = explode("/", $this->factura->getXml());
				$fileName = end($fileName);
				
				$zipArchive->addFromString(
					$fileName, 
					$this->objectStorageService->getStream(
						$this->factura->getXml()
					)
				);		
			}
		}
		$zipArchive->close();			
		$this->download_send_headers($filename);
	}

	/**
	 * Crea un archivo zip, agrega todos los xml's de las facturas y envia el archivo para su descarga
	 * @param \com\descarga\Descarga $descarga
	 * @return void
	 */
	public function descargarArchivosZip(array $idEmpresas)
	{			
		ini_set('memory_limit', '-1');
		
		$zipArchive = new ZipArchive();
		$filename = date("Ymd_His")."_boveda.zip";
		
		if($zipArchive->open(self::RUTA_DOWNLOADS.$filename, ZipArchive::CREATE) !== true)
			throw new Exception("No se pudo crear el archivo");

		$listDescargas = [];
		foreach($idEmpresas as $id){
			$descarga = new Descarga();
			$descarga->setIdEmpresa($id);
			array_push($listDescargas, $descarga);
		}		
		if(!empty($listDescargas)) 
			$listDescargas = $this->consultarEn($listDescargas);
			
		$this->logger->info($listDescargas);
		
		foreach($listDescargas as $key => $val){
			$this->factura->__fromArray($val);
			
			$fileName = explode("/", $this->factura->getXml());
			$fileName = end($fileName);
			
			$zipArchive->addFromString(
				$fileName, 
				$this->objectStorageService->getStream(
					$this->factura->getXml()
				)
			);			
		}			
		$zipArchive->close();
		$this->download_send_headers($filename);
	}

	/**
	 * Consulta una factura para descargar
	 * @param \com\descarga\Descarga $descarga
	 * @return array
	 */
	public function consultar($descarga)
	{
		return $this->descargaDao->consultar($descarga);
	}

	public function consultarEn(array $listDescargas)
	{
		return $this->descargaDao->consultarEn($listDescargas);
	}

	/**
	 * Permite la descarga de un archivo de tipo ZIP
	 * @param string $filename
	 * @return void
	 */
	public function download_send_headers($filename) 
	{
		set_time_limit(0);			
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: application/force-download");
		header("Content-type: application/zip");			
		header('Set-Cookie: fileDownload=true; path=/');
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=".$filename);
		readfile(self::RUTA_DOWNLOADS.$filename);
		unlink(self::RUTA_DOWNLOADS.$filename);
	}
}
