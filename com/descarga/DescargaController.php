<?php
/**
 * Controlador para el recurso Descarga
 */
namespace com\descarga;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso Descarga, atiende a las peticiones que contengas /mvc/descarga
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Descarga
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/descarga})
 */
class DescargaController
{
	/** @Resource(name=DescargaService) */
	protected $descargaService;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso descarga/descargar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function descargarAction($cfdis)
	{
		$this->logger->info("Atendiendo la peticion /descarga/descargar");
		$this->descargaService->descargarZip($cfdis);
	}

	/**
	 * Atiende peticiones al recurso descarga/descargarArchivos
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function descargarArchivosAction($idEmpresas)
	{
		$this->logger->info("Atendiendo la peticion /descarga/descargarArchivos");
		$this->descargaService->descargarArchivosZip(json_decode($idEmpresas, true));
	}
}
