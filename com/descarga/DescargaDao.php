<?php
/**
 * DAO's para el recurso Descarga
 */
namespace com\descarga;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso Descarga
 *
 * @author Israel Hernándex
 * @package com
 * @subpackage com\descarga
 * @version 1.0
 *
 * @Component(name=DescargaDao)
 * @Singleton
 */
class DescargaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
	
	public function consultarEn(array $listDescargas)
	{
		return $this->sqlMapperService->ejecutarDao($listDescargas, 'consultarEn');
	}
}
