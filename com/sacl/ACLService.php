<?php
/**
 * Servicios del componente ACL
 */
namespace com\sacl;

use MNIComponents\Base\TService;


/**
 * Servicios del componente ACL
 *
 * @author 		Israel Hernández
 * @category	Service
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=ACLService)
 * @Singleton
 */
class ACLService
{
	/** @Resource=(name=ACL) */
	protected $acl;
	/** @Resource=(name=ACLDao) */
	protected $aclDao;
	/** @Resource=(name=UsuarioRol) */
	protected $usuarioRol;
	/** @Resource=(name=UsuarioRolDao) */
	protected $usuarioRolDao;	
	protected $logger;
	use TService;

	/**
	 * Método que permite asegurar el acceso a alguna acción sobre un recurso
	 * @param \com\usuario\Usuario $usuario
	 * @param \com\sacl\Recurso||string $recurso
	 * @return boolean
	 * @throws Exception
	 */
	public function isAllowed($sesion, $recurso)
	{
		$permiso = false;

		$this->usuarioRol->setIdUsuario($sesion->getIdUsuario());
		$roles = $this->usuarioRolDao->consultar($this->usuarioRol);

		if(is_object($recurso))
			$this->acl->setRecurso($recurso->getIdRecurso());
		else
			$this->acl->setRecurso($recurso);

		foreach($roles as $key => $val){
			$this->acl->setRol($val["idRol"]);
			$list = $this->aclDao->consultar($this->acl);
			if(!empty($list)){
				if(is_array($list[0]) && $list[0]["permiso"] == 1){
					$permiso = true;
					break;
				}
			}
		}
		if(!$permiso)
			throw new \Exception("No cuenta con los permisos para realizar esta acción, contacte con su administrador.");		

		return $permiso;
	}
}
