<?php
/**
 * Servicios del componente Menu
 */
namespace com\sacl\menu;

use MNIComponents\Base\TService;


/**
 * Servicios del componente Menu
 *
 * @author 		Israel Hern�ndez
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Menu
 * @version 	1.1
 * 
 * @Component(name=MenuService)
 * @Singleton
 */
class MenuService
{
	/** @Resource(name=MenuDao) */
	protected $menuDao;
	protected $logger;
	use TService;

	/**
	 * Consulta de un menu determinado
	 * @param Menu $menu
	 * @return array
	 */
	public function consultar($menu)
	{
		return $this->menuDao->consultar($menu);
	}
}
