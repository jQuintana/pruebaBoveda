<?php
/**
 * Modelo de la clase Menu
 */
namespace com\sacl\menu;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Menu
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=Menu)
 * @Prototype
 */
class Menu extends BModel
{
	private $idMenu;
	private $alias;
	private $url;
	private $padre;
	private $class;
	use TModel;

	public function getIdMenu()
	{
		return $this->idMenu ;
	}
	
	public function setIdMenu($idMenu)
	{
		$this->idMenu = $idMenu;
	}
	
	public function getAlias()
	{
		return $this->alias ;
	}
	
	public function setAlias($alias)
	{
		$this->alias = $alias;
	}
	
	public function getUrl()
	{
		return $this->url ;	
	}
	
	public function setUrl($url)
	{
		$this->url = $url;
	}
	
	public function getPadre()
	{
		return $this->padre ;
	}

	public function setPadre($padre)
	{
		$this->padre = $padre;
	}
	
	public function getClass()
	{
		return $this->class ;
	}
	
	public function setClass($class)
	{
		$this->class = $class;
	}
}
