<?php
/**
 * Servicios del componente Opcion
 */
namespace com\sacl\opcion;

use MNIComponents\Base\TService;


/**
 * Servicios del componente Opcion
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=OpcionService)
 * @Singleton
 */
class OpcionService
{
	/** @resource(name=OpcionDao) */
	protected $opcionDao;
	protected $logger;
	use TService;

	/**
	 * Consulta de opciones
	 * @param \com\sacl\Opcion $opcion
	 * @return array
	 */
	public function consultar($cpcion)
	{
		return $this->opcionDao->consultar($cpcion);
	}
}
