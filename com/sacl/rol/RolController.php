<?php
/**
 * Controlador para el recurso Rol
 */
namespace com\sacl\rol;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/rol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/rol})
 */
class RolController
{
	/** @Resource(name=Rol) */
	protected $rol;
	/** @Resource(name=RolService) */
	protected $rolService;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso rol/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /rol/listar");
		$response = $this->rolService->listar($this->rol);
		$this->response($response);
	}
}
