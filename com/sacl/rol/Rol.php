<?php
/**
 * Modelo de la clase Rol
 */
namespace com\sacl\rol;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Rol
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=Rol)
 * @Prototype
 */
class Rol extends BModel
{
	private $idRol;
	private $estatus;
	private $descripcion;
	use TModel;

	public function getIdRol()
	{
		return $this->idRol;
	}
	
	public function setIdRol($idRol)
	{
		$this->idRol = $idRol;
	}
	
	public function getEstatus()
	{
		return $this->estatus;
	}
	
	public function setEstatus($estatus)
	{
		$this->estatus = $estatus;
	}
	
	public function getDescripcion()
	{
		return $this->descripcion;
	}
	
	public function setDescripcion($descripcion)
	{
		$this->descripcion = $descripcion;
	}
}
