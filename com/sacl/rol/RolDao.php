<?php
/**
 * DAO's para el recurso Rol
 */
namespace com\sacl\rol;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Rol
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=RolDao)
 * @Singleton
 */
class RolDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
