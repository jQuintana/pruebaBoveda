<?php
/**
 * Modelo de la clase Usuario Rol
 */
namespace com\sacl\usuariorol;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Usuario Rol
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=UsuarioRol)
 * @Prototype
 */
class UsuarioRol extends BModel
{
	private $idRol;
	private $idUsuario;
	use TModel;

	public function getIdRol()
	{
		return $this->idRol;
	}
	
	public function setIdRol($idRol)
	{
		$this->idRol = $idRol;
	}
	
	public function getIdUsuario()
	{
		return $this->idUsuario;
	}
	
	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}
}
