<?php
/**
 * Servicios del componente UsuarioRol
 */
namespace com\sacl\usuariorol;

use MNIComponents\Base\TService;
use com\sesion\Sesion;


/**
 * Servicios del componente UsuarioRol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @package 	ACL
 * @version 	1.1
 * 
 * @Component(name=UsuarioRolService)
 * @Singleton
 */
class UsuarioRolService
{
	/** @Resource(name=UsuarioRolDao) */
	protected $usuarioRolDao;
	/** @Resource(name=SesionService) */
	protected $sesionService;
	/** @Resource(name=ACLService) */
	protected $aclService;
	protected $logger;	
	use TService;

	/**
	 * M�todo que permite consultar una relacion usuario/rol
	 * @param UsuarioRol $usuarioRol
	 * @return array
	 */
	public function consultar(UsuarioRol $usuarioRol)
	{
		return $this->usuarioRolDao->consultar($usuarioRol);
	}

	/**
	 * M�todo que permite eliminar una relacion usuario/rol
	 * @param UsuarioRol $usuarioRol
	 * @param Sesion $sesion
	 * @return array
	 * @throws Exception
	 */
	public function eliminar(UsuarioRol $usuarioRol, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->usuarioRolDao->eliminar($usuarioRol);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * M�todo que permite insertar una nueva relacion usuario/rol
	 * @param UsuarioRol $usuarioRol
	 * @param Sesion $sesion
	 * @throws Exception
	 * @return array
	 */
	public function insertar(UsuarioRol $usuarioRol, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->usuarioRolDao->insertar($usuarioRol);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * M�todo que permite listar las relaciones usuario/rol
	 * @param UsuarioRol $usuarioRol
	 * @return array
	 */
	public function listar(UsuarioRol $usuarioRol)
	{
		return $this->usuarioRolDao->listar($usuarioRol);
	}
}
