<?php
/**
 * DAO's del recursp UsuarioRol
 */
namespace com\sacl\usuariorol;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso UsuarioRol
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=UsuarioRolDao)
 * @Singleton
 */
class UsuarioRolDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
}
