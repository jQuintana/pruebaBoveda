<?php
/**
 * Controlador para el recurso UsuarioRol
 */
namespace com\sacl\usuariorol;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/usuarioRol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/usuarioRol})
 */
class UsuarioRolController
{
	/** @Resource(name=Rol) */
	protected $rol;
	/** @Resource(name=RolService) */
	protected $rolService;
	/** @Resource(name=Usuario) */
	protected $usuario;
	/** @Resource(name=UsuarioService) */
	protected $usuarioService;
	/** @Resource(name=UsuarioRolService) */
	protected $usuarioRolService;
	/** @Resource(name=UsuarioRol) */
	protected $usuarioRol;
	/** @Resource(name=Sesion) */
	protected $sesion;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso usuarioRoll/consultar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function consultarAction($usuario)
	{
		$this->logger->info("Atendiendo la peticion /usuarioRol/consultar");
		$this->usuario->__fromJson($usuario);
		$this->usuarioRol->setIdUsuario($this->usuario->getUsuario());
		$response = $this->usuarioRolService->consultar($this->usuarioRol);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso usuarioRoll/Eliminar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function eliminarAction($usuario, $rol, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuarioRol/eliminar");
		$this->usuario->__fromJson($usuario);
		$this->rol->__fromJson($rol);
		$this->sesion->__fromJson($sesion);
		$this->usuarioRol->setIdUsuario($this->usuario->getUsuario());
		$this->usuarioRol->setIdRol($this->rol->getIdRol());
		$response = $this->usuarioRolService->eliminar($this->usuarioRol, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso usuarioRoll/isertar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function insertarAction($usuario, $rol, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuarioRol/insertar");
		$this->usuario->__fromJson($usuario);
		$this->rol->__fromJson($rol);
		$this->sesion->__fromJson($sesion);
		$this->usuarioRol->setIdUsuario($this->usuario->getUsuario());
		$this->usuarioRol->setIdRol($this->rol->getIdRol());
		$response = $this->usuarioRolService->insertar($this->usuarioRol, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso usuarioRoll/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction($usuario)
	{
		$this->logger->info("Atendiendo la peticion /usuarioRol/listar");
		$this->usuario->__fromJson($usuario);
		$this->usuarioRol->setIdUsuario($this->usuario->getUsuario());
		$response = $this->usuarioRolService->listar($this->usuarioRol);
		$this->response($response);
	}
}
