<?php
/**
 * DAO de la clase ACL
 */
namespace com\sacl;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * Modelo de la clase Menu
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Dao
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=ACLDao)
 * @Singleton
 */
class ACLDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
