<?php
/**
 * DAO's del recurso Recurso
 */
namespace com\sacl\recurso;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Recurso
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *  
 * @Component(name=RecursoDao)
 * @Singleton
 */
class RecursoDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
