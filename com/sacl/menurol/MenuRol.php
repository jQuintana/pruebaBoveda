<?php
/**
 * Modelo de la clase MenuRol
 */
namespace com\sacl\menurol;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase MenuRol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=MenuRol)
 * @Prototype
 */
class MenuRol extends BModel
{
	private $idMenu;
	private $idRol;
	use TModel;

	public function getIdMenu()
	{
		return $this->idMenu ;
	}
	
	public function setIdMenu($idMenu)
	{
		$this->idMenu = $idMenu;
	}
	
	public function getIdRol()
	{
		return $this->idRol ;
	}
	
	public function setIdRol($idRol)
	{
		$this->idRol = $idRol;
	}
}
