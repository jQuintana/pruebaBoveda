<?php
/**
 * Controlador para el recurso MenuRol
 */
namespace com\sacl\menurol;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/menuRol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/menuRol})
 */
class MenuRolController
{
	/** @Resource(name=MenuRolService) */
	protected $menuRolService;
	/** @Resource(name=Menu) */
	protected $menu;
	/** @Resource(name=Usuario) */
	protected $usuario;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso menuRol/consultar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function consultarAction($usuario, $alias)
	{
		$this->logger->info("Atendiendo la peticion /menuRol/consultar");
		$this->usuario->__fromJson($usuario);
		$this->menu->__fromJson($alias);
		$response = $this->menuRolService->consultar($this->usuario, $this->menu);
		$this->response($response);
	}
}
