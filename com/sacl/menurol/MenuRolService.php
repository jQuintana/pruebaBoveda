<?php
/**
 * Servicios del componente MenuRol
 */
namespace com\sacl\menurol;

use MNIComponents\Base\TService;
use com\usuario\Usuario;
use com\sacl\menu\Menu;


/**
 * Servicios del componente MenuRol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=MenuRolService)
 * @Singleton
 */
class MenuRolService
{
	/** @Resource(name=UsuarioRolService) */
	protected $usuarioRolService;
	/** @Resource(name=UsuarioRol) */
	protected $usuarioRol;
	/** @Resource(name=MenuService) */
	protected $menuService;
	/** @Resource(name=MenuRol) */
	protected $menuRol;
	/** @Resource(name=MenuRol) */
	protected $subMenuRol;
	/** @Resource(name=MenuRolDao) */
	protected $menuRolDao;
	/** @Resource(name=Opcion) */
	protected $opcion;
	/** @Resource(name=OpcionService) */
	protected $opcionService;
	protected $logger;
	use TService;

	/**
	 * Metodo que permite obtener las opciones de un menu permitidas para cierto usuario de acuerdo
	 * a sus roles
	 * @param \com\usuario\Usuario $usuario
	 * @param string $menu
	 * @param array $estructuraMenu
	 * @return array
	 * @throws Exception
	 */
	public function consultar(Usuario $usuario, Menu $menu, $estructuraMenu = array()){

		$menuRol = new MenuRol();
		$this->usuarioRol->setIdUsuario($usuario->getUsuario());
		$listRol = $this->usuarioRolService->consultar($this->usuarioRol);

		$listMenu = $this->menuService->consultar($menu);
		if(!empty($listMenu)){
			$menu->__fromArray($listMenu[0]);
			$menuRol->setIdMenu($menu->getIdMenu());

			$options = array();
			$submenu = array();
			foreach($listRol as $key => $val){
				$menuRol->setIdRol($val["idRol"]);
				$listMenuRol = $this->menuRolDao->consultar($menuRol);
				if(!empty($listMenuRol)){
					$alias = "";
					foreach($listMenuRol as $array){
						$alias = $array["alias"];
						$estructuraMenu[$array["alias"]] = $array;
					}

					$menu->__fromArray($listMenuRol[0]);
					$this->opcion->setMenu($menu->getIdMenu());
					$listOpcion = $this->opcionService->consultar($this->opcion);
					if(!empty($listOpcion)){
						foreach($listOpcion as $key => $value){
							$options[$value["alias"]] = $value;
						}
					}

					$estructuraMenu[$alias]["options"] = $options;

					foreach($options as $key => $val){
						$menu->setAlias($val["alias"]);
						$listMenuRol = $this->consultar($usuario, $menu);
						if(!empty($listMenuRol)){
							$submenu[$val["alias"]] = $listMenuRol;
						}elseif($val["url"] == "")
							unset($estructuraMenu[$alias]["options"][$key]);
					}

					foreach($submenu as $key => $val){
						$estructuraMenu[$alias]["options"][$key] = $val[$key];
					}
				}
			}
		}
		return $estructuraMenu;
	}
}
