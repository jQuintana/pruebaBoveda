<?php
/**
 * Sesion DAO's
 */
namespace com\sesion;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * Sesion DAO's
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	Sesion
 * @version 	1.1
 * 
 * @Component(name=SesionDao)
 * @Singleton
 */
class SesionDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
