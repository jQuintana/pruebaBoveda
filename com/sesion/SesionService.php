<?php
/**
 * Service para el recurso Sesion
 */
namespace com\sesion;

use MNIComponents\Base\TService;
use DateTime;
use Exception;


/**
 * Service para el recurso Sesion
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	Sesion
 * @version 	1.1
 * 
 * @Component(name=SesionService)
 * @Singleton
 */
class SesionService
{
	/** @Resource(name=SesionDao) */
	protected $sesionDao;
	/** @Resource(name=Parametro) */
	protected $parametro;
	/** @Resource(name=ParametroService) */
	protected $parametroService;
	protected $logger;
	use TService;

	/**
	 * Consultar una sesion
	 * @param Sesion $sesion
	 * @return array
	 */
	public function consultar(Sesion $sesion)
	{
		$list = $this->sesionDao->consultar($sesion);
		return (!empty($list)) ? $list[0] : $list;
	}

	/**
	 * Verifica si un token perteneciente a una sesion de un usuario es el que se encuentra en BD y 
	 * si la fecha de expiracion no ha vencido
	 * @param Sesion $sesion
	 * @return boolean
	 */
	public function validar(Sesion $sesion)
	{
		$sesionArray = $this->consultar($sesion);
		$valido = true;
		if(!empty($sesionArray)){
			if(($sesion->getToken() !== $sesionArray["token"]) || $sesion->getToken() == "")
				$valido = false;
			if(new DateTime($sesionArray["fechaExpiracion"]) < new DateTime("now"))
				$valido = false;
		}
		return $valido;
	}

	/**
	 * Genera apartir de un simple algoritmo el token y la fecha de expiracion de una sesion
	 * @param Sesion $sesion
	 * @return Sesion
	 */
	public function generaTokenFechaExpiracion(Sesion $sesion)
	{
		$datetime = date("Y-m-d H:i:s");

		$this->parametro->setParametro("DS");
		$this->parametro = $this->parametroService->consultar($this->parametro);
		
		$duracionSesion = $this->parametro->getValor();
		$fechaExpiracion = strtotime('+'.$duracionSesion.' hour', strtotime($datetime));
		$fechaExpiracion = date ("Y-m-d H:i:s", $fechaExpiracion);
		$sesion->setToken(sha1($sesion->getIdUsuario().$datetime));
		$sesion->setFechaExpiracion($fechaExpiracion);
		return $sesion;
	}

	/**
	 * Persiste una nueva sesion
	 * @param Sesion $sesion
	 * @return mixed
	 */
	public function insertar(Sesion $sesion)
	{
		$sesion = $this->generaTokenFechaExpiracion($sesion);
		return $this->sesionDao->insertar($sesion);
	}

	/**
	 * Actualiza los cambios en una sesion
	 * @param Sesion $sesion
	 * @return boolean
	 */
	public function actualizar(Sesion $sesion)
	{
		$sesion = $this->generaTokenFechaExpiracion($sesion);
		return $this->sesionDao->actualizar($sesion);
	}

	/**
	 * Elimina una sesion
	 * @param Sesion $sesion
	 * @return boolean
	 */
	public function eliminar(Sesion $sesion)
	{
		return $this->sesionDao->eliminar($sesion);
	}

	/**
	 * Se generará un token en caso de que no exista una sesion para el usuario que realiza la 
	 * peticion, de lo contrario actualizara la sesión
	 * @param \com\sesionSesion $sesion
	 * @return array
	 * @throws Exception
	 */
	public function token(Sesion $sesion)
	{
		$list = $this->consultar($sesion);
		if(empty($list))
			$this->insertar($sesion);
		else{
			if($this->validar($sesion))
				$this->actualizar($sesion);
			else{
				$this->eliminar($sesion);
				$this->sesionDao->commit();
				throw new Exception("La sesión ha expirado o es inválida <script> $.localStorage.clear(); setTimeout(function reload(){location.href='';}, 3000); </script>");
			}
		}
		return $this->consultar($sesion);		
	}
}
