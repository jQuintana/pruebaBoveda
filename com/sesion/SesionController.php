<?php
/**
 * Controller para el recurso Sesion
 */
namespace com\sesion;

use MNIComponents\Base\TController;


/**
 * Controller del recurso usuario, atiende a las peticiones que contengas /mvc/sesion
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Sesion
 * @version 	1.1
 *
 * @Controller
 * @RequestMapping(url={/sesion})
 * @Singleton
 */
class SesionController
{
	/** @Resource(name=Sesion) */
	protected $sesion;
	/** @Resource(name=SesionService) */
	protected $sesionService;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso sesion/consultar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function consultarAction($sesion)
	{
		$this->logger->info("Atendiendo la peticion /sesion/consultar");		
		$this->sesion->__fromJson($sesion);
		$response = $this->sesionService->token($this->sesion);
		$this->response($response);
	}
}
