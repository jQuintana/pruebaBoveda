<?php
/**
 * Service para el recurso Usuario Empresa
 */
namespace com\usuario\usuarioempresa;

use MNIComponents\Base\TService;
use com\sesion\Sesion;
use Exception;


/**
 * Service para el recurso Usuario Empresa
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Usuario Empresa
 * @version 	1.1
 *
 * @Component(name=UsuarioEmpresaService)
 * @Singleton
 */
class UsuarioEmpresaService 
{
	/** @Resource(name=UsuarioEmpresaDao) */ 
	protected $usuarioEmpresaDao;
	/** @Resource(name=Sesion) */ 
	protected $sesion;	
	/** @Resource(name=SesionService) */ 
	protected $sesionService;
	/** @Resource(name=ACLService) */ 
	protected $aclService;
	protected $logger;
	use TService;

	/**
	 * Permite eliminar usuarios
	 * @param Usuario $usuario
	 * @param Sesion $sesion
	 * @return array	 
	 */
	public function eliminar(UsuarioEmpresa $usuarioEmpresa, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->usuarioEmpresaDao->eliminar($usuarioEmpresa);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Permite crear un usuario nuevo
	 * @param Usuario $usuarioEmpresa
	 * @param Sesion $sesion
	 * @return array
	 */
	public function insertar(UsuarioEmpresa $usuarioEmpresa, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->usuarioEmpresaDao->insertar($usuarioEmpresa);
			return $this->sesionService->token($sesion);
		}
	}
	
	/**
	 * Consulta un usuario deyerminado
	 * @param Usuario $usuarioEmpresa
	 * @return array
	 */
	public function consultar(UsuarioEmpresa $usuarioEmpresa)
	{
		return $this->usuarioEmpresaDao->consultar($usuarioEmpresa);
	}
}
