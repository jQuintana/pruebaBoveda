<?php
/**
 * DAO's del recurso Usuario Empresa
 */
namespace com\usuario\usuarioempresa;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Usuario Empresa
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAP
 * @package 	Boveda
 * @subpackage 	Usuario Empresa
 * @version 	1.1
 * 
 * @Component(name=UsuarioEmpresaDao)
 * @Singleton
 */
class UsuarioEmpresaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
