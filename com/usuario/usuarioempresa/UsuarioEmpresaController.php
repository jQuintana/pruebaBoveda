<?php
/**
 * Controller para el recurso Usuario Empresa
 */
namespace com\usuario\usuarioempresa;

use MNIComponents\Base\TController;


/**
 * Controller del recurso usuario, atiende a las peticiones que contengas /mvc/usuario
 *
 * @author 		Israel Hernández
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Usuario Empresa
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/usuarioEmpresa})
 */
class UsuarioEmpresaController
{
	/** @Resource(name=UsuarioEmpresa) */
	protected $usuarioEmpresa;
	/** @Resource(name=UsuarioEmpresaService) */
	protected $usuarioEmpresaService;
	/** @Resource(name=Sesion) */	
	protected $sesion;
	protected $jqgridfilter;
	protected $logger;	
	use TController;

	/**
	 * Atiende peticiones al recurso usuario/eliminar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function eliminarAction($usuarioEmpresa, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuarioEmpresa/eliminar");		
		$this->usuarioEmpresa->__fromJson($usuarioEmpresa);
		$this->sesion->__fromJson($sesion);
		$response = $this->usuarioEmpresaService->eliminar($this->usuarioEmpresa, $this->sesion);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso usuario/insertar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function insertarAction($usuarioEmpresa, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuarioEmpresa/insertar");
		$this->usuarioEmpresa->__fromJson($usuarioEmpresa);
		$this->sesion->__fromJson($sesion);		
		$response = $this->usuarioEmpresaService->insertar($this->usuarioEmpresa, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso usuario/consultar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function consultarAction($usuarioEmpresa)
	{
		$this->logger->info("Atendiendo la peticion /usuarioEmpresa/consultar");
		$this->usuarioEmpresa->__fromJson($usuarioEmpresa);
		$response = $this->usuarioEmpresaService->consultar($this->usuarioEmpresa);
		$this->response($response);	
	}
}
