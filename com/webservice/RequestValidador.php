<?php
/**
 * Modelo del Cliente Webservice del validador de XML
 */
namespace com\webservice;

use MNIComponents\Base\TModel;


/**
 * Modelo del Cliente Webservices del validador de XML
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Webservice
 * @version 	1.1
 */
class RequestValidador
{
	private $wsdl;
	private $attemps;
	private $token;
	use TModel;

	public function getWsdl()
	{
		return $this->wsdl;
	}
	
	/** @Required */
	public function setWsdl($wsdl)
	{
		$this->wsdl = $wsdl;
	}

	/** @Required */
	public function setAttempts($attemps)
	{
		$this->attemps = $attemps;
	}

	public function getAttempts()
	{
		return $this->attemps;
	}

	public function getToken()
	{
		return $this->token;
	}
	
	/** @Required */
	public function setToken($token)
	{
		$this->token = $token;
	}	
}
