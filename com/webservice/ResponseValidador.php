<?php
/**
 * Modelo del Cliente Webservice del validador de XML
 */
namespace com\webservice;

use MNIComponents\Base\TModel;


/**
 * Modelo del Cliente Webservices del validador de XML
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Webservice
 * @version 	1.1
 */
class ResponseValidador
{
	private $codigo;
	private $mensaje;
	use TModel;

	public function getCodigo()
	{
		return $this->codigo;
	}
	
	public function setCodigo($codigo)
	{
		$this->codigo = $codigo;
	}

	public function getMensaje()
	{
		return $this->mensaje;
	}
	
	public function setMensaje($mensaje)
	{
		$this->mensaje = $mensaje;
	}
}
