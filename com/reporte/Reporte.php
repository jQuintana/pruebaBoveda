<?php
/**
 * Modelo de la clase Reporte
 */
namespace com\reporte;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Reporte
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Reporte
 * @version 	1.1
 *
 * @Component(name=Reporte)
 * @Prototype
 */		
class Reporte extends BModel
{
	private $nombre;
	private $idEmpresa;		
	use TModel;
	
	public function getNombre()
	{
		return $this->nombre ;
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getIdEmpresa()
	{
		return $this->idEmpresa ;
	}
	
	public function setIdEmpresa($idEmpresa)
	{
		$this->idEmpresa = $idEmpresa;
	}
}	
