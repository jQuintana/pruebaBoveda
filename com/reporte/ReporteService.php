<?php
/**
 * Servicios del recurso Reporte
 */
namespace com\reporte;

use MNIComponents\Base\TService;


/**
 * Servicios del recurso Reporte
 * 
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	Reporte
 * @version 	1.1
 * 
 * @Component(name=ReporteService)
 * @Singleton
 */
class ReporteService
{
	/** @Resource(name=ReporteDao) */
	protected $reporteDao;
	protected $logger;
	use TService;

	/**
	 * Genera un reporte en CSV a partir del arreglo de una consulta
	 * @param \com\reporte\Repore $reporte
	 * @param \com\jqgridfilter\JQGridFilter $jqgridfilter
	 * @return void
	 */
	public function descargar(array $idEmpresas)
	{
		unset($_REQUEST["idEmpresas"]);
		$_REQUEST = ["_search"  => true] + $_REQUEST;
		$_REQUEST["nd"] = mt_rand();			
		$_REQUEST["rows"] = 100000;			
		$_REQUEST["page"] = 1;		
		
		$listReporte = [];
		foreach($idEmpresas as $id){
			$reporte = new Reporte();
			$reporte->setIdEmpresa($id);
			array_push($listReporte, $reporte);
		}
		
		if(!empty($listReporte)){	
			$listReporte = $this->consultarEn($listReporte);			
			$this->download_send_headers("reporte_".date("Y-m-d").".csv");
			die($this->array2csv($listReporte["rows"]));
		}
	}

	/**
	 * Realiza la consulta de datos para crear un reporte
	 * @param \com\reporte\Repore $reporte
	 * @param \com\jqgridfilter\JQGridFilter $jqgridfilter
	 * @return array
	 */
	public function consultarEn($reporte)
	{
		return $this->reporteDao->consultarEn($reporte);
	}

	/**
	 * Genera un archivo CSV apartir de un array
	 * @param array $array
	 * @return file
	 */
	public function array2csv($array)
	{
		if(empty($array))
			return null;
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($array)));
		foreach ($array as $row) {
			fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}

	/**
	 * Permite la descarga de un archivo CSV
	 * @param string $filename
	 * @return void
	 */
	public function download_send_headers($filename) 
	{
		set_time_limit(0);
		header("Pragma: public");
		header("Expires: 0");
		header("Last-Modified: ". gmdate("D, d M Y H:i:s") ." GMT");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: application/force-download");
		header('Set-Cookie: fileDownload=true; path=/');
		header("Content-type: text/csv");			
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");			
		header("Content-Disposition: attachment; filename={$filename}");
	}
}
