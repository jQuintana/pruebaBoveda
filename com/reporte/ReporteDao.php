<?php
/**
 * DAO's para el recurso Reporte
 */
namespace com\reporte;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso Reporte
 *
 * @author 		Israel Hernándex <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	Reporte
 * @version 	1.1
 * 
 * @Component(name=ReporteDao)
 * @Singleton
 */
class ReporteDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
	
	public function consultarEn(array $listReporte)
	{
		return $this->sqlMapperService->ejecutarDao($listReporte, 'consultarEn');
	}
}
