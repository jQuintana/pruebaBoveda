<?php
/**
 * Modelo de la clase Parametro
 */
namespace com\parametro;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Parametro
 *
 * @author 		Israel Hern�ndez
 * @category	Model
 * @package 	Bovedacom
 * @subpackage 	Parametro
 * @version 	1.1
 *
 * @Component(name=Parametro)
 * @Prototype
 */
class Parametro extends BModel
{
	private $parametro;
	private $descripcion;
	private $valor;
	private $unidadMedida;
	private $notificacion;
	private $horaModificacion;
	private $usuarioModificacion;
	use TModel;

	public function getParametro()
	{
		return $this->parametro ;
	}
	
	public function setParametro($parametro)
	{
		$this->parametro = $parametro;
	}
	
	public function getDescripcion()
	{
		return $this->descripcion ;
	}
	
	public function setDescripcion($descripcion)
	{
		$this->descripcion = $descripcion;
	}
	
	public function getValor()
	{
		return $this->valor ;
	}
	
	public function setValor($valor)
	{
		$this->valor = $valor;
	}
	
	public function getUnidadMedida()
	{
		return $this->unidadMedida ;
	}

	public function setUnidadMedida($unidadMedida)
	{
		$this->unidadMedida = $unidadMedida;
	}
	
	public function getNotificacion()
	{
		return $this->notificacion ;
	}
	
	public function setNotificacion($notificacion)
	{
		$this->notificacion = $notificacion;
	}
	
	public function getHoraModificacion()
	{
		return $this->horaModificacion ;
	}
	
	public function setHoraModificacion($horaModificacion)
	{
		$this->horaModificacion = $horaModificacion;
	}
	
	public function getUsuarioModificacion()
	{
		return $this->usuarioModificacion ;
	}
	
	public function setUsuarioModificacion($usuarioModificacion)
	{
		$this->usuarioModificacion = $usuarioModificacion;
	}
}
