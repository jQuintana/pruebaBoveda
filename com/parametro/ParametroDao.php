<?php
/**
 * DAO's para el recurso Parametro
 */
namespace com\parametro;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso Parametro
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO 
 * @package 	Bodeda
 * @subpackage 	Parametro
 * @version 	1.1
 * 
 * @Component(name=ParametroDao)
 * @Singleton
 */
class ParametroDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
}
