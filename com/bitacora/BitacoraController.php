<?php
/**
 * Controlador para el recurso Bitacora
 */
namespace com\bitacora;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso bitacora, atiende a las peticiones que contengas /mvc/bitacora
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category  	Controller	
 * @package 	Boveda
 * @subpackage 	Bitacora
 * @version 	1.1
 *
 * @Controller
 * @RequestMapping(url={/bitacora}) 
 * @Singleton
 */
class BitacoraController
{
	/** @Resource(name=Bitacora) */
	protected $bitacora;
	/** @Resource(name=BitacoraService) */
	protected $bitacoraService;
	/** @Resource(name=Usuario) */
	protected $usuario;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso bitacora/listar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /bitacora/listar");
		$response = $this->bitacoraService->listar($this->bitacora);
		$this->response($response);		
	}
}
