<?php
/**
 * Servicios para el recurso bitacora
 */
namespace com\bitacora;

use MNIComponents\Base\TService;


/**
 * Servicios del recurso bitacora
 *
 * @author 		Israel Hernández
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	Bitacora
 * @version 	1.1
 *
 * @Component(name=BitacoraService)
 * @Singleton
 */
class BitacoraService
{
	/** @Resource(name=BitacoraDao) */
	protected $bitacoraDao;	
	protected $logger;
	use TService;

	/**
	 * Servicio que lista lso registros de la bitacora
	 * @param \com\bitacora\Bitacora $bitacora
	 * @param \com\jqgridfilter\JQGridFilter $jqgridfilter
	 * @return array
	 */
	public function listar($bitacora)
	{
		return $this->bitacoraDao->listar($bitacora);
	}

	/**
	 * Servicio de insercion de un nuevo registro en la bitacora
	 * @param \com\bitacora\Bitacora $bitacora
	 * @return boolean
	 */
	public function insertar($bitacora)
	{
		return $this->bitacoraDao->insertar($bitacora);		
	}

	/**
	 * Servicio de registro de un nuevo evento en la bitacora por usuario
	 * @param \com\bitacora\Bitacora $evento
	 * @param \com\usuario\Usuario $usuario
	 * @return boolean
	 */
	public function anotaBitacora($bitacora)
	{				
		$mensaje = utf8_encode(preg_replace("/[\t|\n|\r|\n\r]/i", " ", $bitacora->getMensaje()));
		$bitacora->setMensaje($mensaje);
		$bitacora->setHora(date("Y-m-d h:i:s"));			
		return $this->insertar($bitacora);
	}
}
