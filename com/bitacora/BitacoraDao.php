<?php
	/**
 * DAO's del recurso Bitacora
 */
namespace com\bitacora;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Bitacora
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category 	Dao
 * @package 	Boveda
 * @subpackage 	Bitacora
 * @version 	1.0
 *
 * @Component(name=BitacoraDao)
 * @Singleton
 */
class BitacoraDao implements IDAO 
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDAO;
}
