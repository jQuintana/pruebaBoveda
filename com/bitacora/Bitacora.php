<?php
/**
 * Modelo de la clase Bitacora
 */
namespace com\bitacora;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Bitacora
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category 	Model
 * @package 	Boveda
 * @subpackage 	Bitacora
 * @version 	1.1
 * 
 * @Component(name=Bitacora)
 * @Prototype
 */
class Bitacora extends BModel
{
	private $idBitacora;
	private $evento;
	private $mensaje;
	private $hora;
	private $usuario;
	use TModel;

	public function getIdBitacora()
	{
		return $this->idBitacora;
	}

	public function setIdBitacora($idBitacora)
	{
		$this->idBitacora = $idBitacora;
	}

	public function getEvento()
	{
		return $this->evento;
	}

	public function setEvento($evento)
	{
		$this->evento = $evento;
	}

	public function getMensaje()
	{
		return $this->mensaje;
	}

	public function setMensaje($mensaje)
	{
		$this->mensaje = $mensaje;
	}

	public function getHora()
	{
		return $this->hora;
	}

	public function setHora($hora)
	{
		$this->hora = $hora;
	}

	public function getUsuario()
	{
		return $this->usuario;
	}

	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;
	}
}
