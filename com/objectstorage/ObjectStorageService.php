<?PHP
/**
 * Servicios del recurso ObjectStorage
 */
namespace com\objectstorage;

use MNIComponents\Base\TService;
use com\objectstorage\Upload\Upload;
use OpenCloud\Rackspace;
use	OpenCloud\ObjectStore\Exception\ObjectNotFoundException;
use	OpenCloud\ObjectStore\Resource\DataObject;
use	OpenCloud\ObjectStore\Resource\Container;
use	OpenCloud\Common\Exceptions\AsyncTimeoutError;
use	OpenCloud\Common\Exceptions\NoNameError;
use	Guzzle\Http\Exception\CurlException;
use Logger;


/**
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Servicios
 * @package 	Boveda
 * @subpackage 	ObjectStorage
 * @version 	1.0
 *
 * Servicios del recurso ObjectStorage
 *
 * @Component(name=ObjectStorageService)
 * @Singleton
 */
 class ObjectStorageService
 {
 	/** @Resource(name=ObjectStorage) */
	protected $objectStorage;
	protected $rackspace;
	protected $service;	
	protected $container;	
	protected $logger;
	use TService;
	
	public function conectarRackspace()
	{
		if(!$this->container instanceof Container){
			$this->logger->info("Conectando con el servicio de Rackspace: ".Rackspace::US_IDENTITY_ENDPOINT);		
			$this->rackspace = new Rackspace(
				Rackspace::US_IDENTITY_ENDPOINT,
				array(
					'username'	=> $this->objectStorage->getUsername(),
					'apiKey'	=> $this->objectStorage->getKey()
				)
			);

			$cacheFile = __DIR__.'/.opencloud_token';

			if (file_exists($cacheFile)){
				$this->logger->info("Importando credenciales desde Cache");		
				$data = unserialize(file_get_contents($cacheFile));
				$this->rackspace->importCredentials($data);
			}

			$token = $this->rackspace->getTokenObject();
			if (!$token || ($token && $token->hasExpired())) {
				$this->logger->info("Authenticando y exportando credenciales a cache");		
				$this->rackspace->authenticate();
				file_put_contents($cacheFile, serialize($this->rackspace->exportCredentials()));
			}		

			$this->logger->info("Servicio de ObjectStorage (CloudFiles)");		
			$this->service = $this->rackspace->objectStoreService("cloudFiles", $this->objectStorage->getRegion());	

			$this->logger->info("Obetniendo el contenedor asignado a Boveda2 y habilitando funciones CDN");		
			$existeContenedor = false;
			$containers = $this->service->listContainers();
			foreach ($containers as $container) {
				if($container->getName() == $this->objectStorage->getContainer())					
					$existeContenedor = true;
			}
			if(!$existeContenedor){
				$this->logger->info("No existe el contenedor se creara con el siguiente nombre: ".$this->objectStorage->getContainer());		
				$this->service->createContainer($this->objectStorage->getContainer());
			}
			$this->container = $this->service->getContainer($this->objectStorage->getContainer());						
			$this->container->enableCdn();		
			//$this->cleanContainer();
		}
	}

	private function cleanContainer()
	{
		$this->conectarRackspace();
		$this->logger->info("Limpinado un contenedor para iniciar a produccion");		
		$objects = $this->container->objectList();
		foreach ($objects as $object) {
			$this->deleteObject($object->getName());
		}
	}
	
	public function uploadObject(Upload $upload, $path, array $configuracionPath, $autoUnique = false)
	{
		try{
			$this->conectarRackspace();
			$this->logger->info("Enviando un archivo al contenedor");		

			$remotePath = $this->objectStorage->getPaths()[$path];
			foreach($configuracionPath as $key => $val){
				$remotePath = str_replace("#{".$key."}", $val, $remotePath);
			}

			$configuracionPath["fecha"] = date("Y-m-d H:i:s");		
			$metadataHeaders = DataObject::stockHeaders($configuracionPath);
			$allHeaders = array() + $metadataHeaders; 
			
			$remoteFileName = ($autoUnique) ? date("Ymd_His")."_".mt_rand()."_".$upload->getName() : $upload->getName() ;
			$fileData = fopen($upload->getTmp_name(), 'r');
			
			$this->container->uploadObject($remotePath.$remoteFileName, $fileData, $allHeaders);			
			$url = $this->getObjectUrl($remotePath.$remoteFileName);

			$upload->setUrlPublica($url);
			$upload->setReferencia($remotePath.$remoteFileName);		
		}catch(AsyncTimeoutError $e){
			$this->logger->error($e->getMessage());
			throw new ObjectStorageException("Problemas con el servicio de ObjectStorage");
		}catch(NoNameError $e){
			$this->logger->error($e->getMessage());
			throw new ObjectStorageException("Problemas con el servicio de ObjectStorage");
		}catch(CurlException $e){
			$this->logger->error($e->getMessage());
			throw new ObjectStorageException("Problemas con el servicio de ObjectStorage");
		}
		return $upload;		
	}

	public function getObjectUrl($referencia)
	{
		$this->conectarRackspace();
		$this->logger->info("Generando una URL publica para un objecto");				
		$object = $this->container->getPartialObject($referencia);			
		$httpUrl = $object->getPublicUrl();
		return $httpUrl->getScheme()."://".$httpUrl->getHost().$httpUrl->getPath();
	}

	public function getObjectMetadata($referencia) 
	{
		$this->conectarRackspace();
		$this->logger->info("Obteniendo meta-informacion de un objeto");
		$object = $this->container->getPartialObject($referencia);			
		return $object->getMetadata();
	}

	public function getObject($referencia)
	{	
		$this->conectarRackspace();
		$this->logger->info("Obteniendo un objeto");
		$referencia = str_replace("http://", "", $referencia);
		$referencia = explode("/", $referencia);
		$referencia[0] = "";
		$referencia = implode("/", $referencia);

		try{			
			$object = $this->container->getObject($referencia);			
			return $object;
		}catch(ObjectNotFoundException $e){
			$this->logger->error($e->getMessage());
		}
	}

	public function getStream($referencia)
	{
		$this->conectarRackspace();		
		$object = $this->getObject($referencia);
		$objectContent = $object->getContent();
		$objectContent->rewind();
		return stream_get_contents($objectContent->getStream());
	}

	public function deleteObject($referencia)
	{
		$this->conectarRackspace();
		$this->logger->info("Eliminando un objeto");
		if(!empty($referencia)){
			$object = $this->getObject($referencia);		
			if(is_object($object)) $object->delete();
		}
	}
}
