<?php
/**
 * Modelo del Componente Upload
 */
namespace com\objectstorage\upload;

use MNIComponents\Base\TModel;


/**
 * @author		Israel Hernández <iaejean@hotmail.com>
 * @category 	Model
 * @package 	Boveda
 * @subpackage 	Upload
 * @version 	1.0
 * 
 * Modelo del Componente Upload
 *
 * @Component(name=Upload)
 * @Prototype
 */
class Upload
{
	private $name;
	private $type;
	private $size;
	private $path;
	private $error;
	private $tmp_name;
	private $referencia;
	private $urlPublica;	
	use TModel;		

	public function getName()
	{
		return $this->name;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getType()
	{
		return $this->type;
	}
	
	public function setType($type)
	{
		$this->type = $type;
	}

	public function getSize()
	{
		return $this->size;
	}
	
	public function setSize($size)
	{
		$this->size = $size;
	}

	public function getError()
	{
		return $this->error;
	}
	
	public function setError($error)
	{
		$this->error = $error;
	}

	public function getTmp_name()
	{
		return $this->tmp_name;
	}
	
	public function setTmp_name($tmp_name)
	{
		$this->tmp_name = $tmp_name;
	}	

	public function getPath()
	{
		return $this->path;
	}
	
	public function setPath($path)
	{
		$this->path = $path;
	}	

	public function getUrlPublica()
	{
		return $this->urlPublica;
	}
	
	public function setUrlPublica($urlPublica)
	{
		$this->urlPublica = $urlPublica;
	}	

	public function getReferencia()
	{
		return $this->referencia;
	}
	
	public function setReferencia($referencia)
	{
		$this->referencia = $referencia;
	}			
}
