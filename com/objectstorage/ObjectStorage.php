<?PHP
/**
 * Modelo para el recurso ObjectStorage
 */
namespace com\objectstorage;

use MNIComponents\Base\TModel;


/**
 * @author 		Israel Hernández
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ObjectStorage
 * @version 	1.0
 *
 * Modelo para el recurso ObjectStorage
 */
class ObjectStorage
{
	private $key;
	private $username;
	private $account;
	private $container;
	private $region;
	private $paths;
	use TModel;

	public function setKey($key)
	{
		$this->key = $key;
	}

	public function getKey()
	{
		return $this->key;
	}

	public function setUsername($username)
	{
		$this->username = $username;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function setAccount($account)
	{
		$this->account = $account;
	}

	public function getAccount()
	{
		return $this->account;
	}

	public function setContainer($container)
	{
		$this->container = $container;
	}

	public function getContainer()
	{
		return $this->container;
	}

	public function setRegion($region)
	{
		$this->region = $region;
	}

	public function getRegion()
	{
		return $this->region;
	}	

	public function setPaths($paths)
	{
		$this->paths = $paths;
	}

	public function getPaths()
	{
		return $this->paths;
	}	
}
