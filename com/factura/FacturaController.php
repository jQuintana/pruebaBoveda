<?php
/**
 * Controlador para el recurso Factura
 */
namespace com\factura;

use MNIComponents\Base\TController;
use Ding\Mvc\ModelAndView;
use PHPQRCode\QRcode;


/**
 * Controlador del recurso factura, atiende a las peticiones que contengas /mvc/factura
 *
 * @author 		Israel Hern�ndez
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Factura
 * @version 	1.1
 *
 * @Controller
 * @RequestMapping(url={/factura})
 * @Singleton
 */
class FacturaController
{
	/** @resource(name=Factura) */
	protected $factura;
	/** @resource(name=FacturaService) */
	protected $facturaService;
	/** @Value(value=${conf.directory.tmp}) */
	protected $directoryTmp;
	protected $logger;
	use TController;
	
	/**
	 * Atiende peticiones al recurso factura/subir
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function subirAction($idProveedor, $correoElectronico, $noOrdenCompra)
	{
		$this->logger->info("Atendiendo la peticion /factura/subir");
		$this->factura->__fromJson(
			'{ "idProveedor" : "'.$idProveedor.'"
				, "correoElectronico" : "'.$correoElectronico.'" 
				, "noOrdenCompra" : "'.$noOrdenCompra.'"
			}'
		);
		$response = $this->facturaService->validar($this->factura, $_FILES);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso factura/listarEn
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarEnAction($idEmpresas)
	{
		$this->logger->info("Atendiendo la peticion /facturaEn/listar");		
		$response = $this->facturaService->listarEn(json_decode($idEmpresas, true));
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso factura/actualizar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function actualizarAction($factura)
	{
		$this->logger->info("Atendiendo la peticion /factura/actualizar");
		$this->factura->__fromJson($factura);
		$response = $this->facturaService->actualizar($this->factura);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso factura/subirZip
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function subirZipAction($idProveedor, $correoElectronico, $noOrdenCompra)
	{
		$this->logger->info("Atendiendo la peticion /factura/subirZip");
		$this->factura->__fromJson(
			'{ "idProveedor" : "'.$idProveedor.'"
			, "correoElectronico" : "'.$correoElectronico.'" 
			, "noOrdenCompra" : "'.$noOrdenCompra.'" 
			}'
		);
		$response = $this->facturaService->validarZip($this->factura, $_FILES);
		$this->response($response);
	}
	public function visualizaXmlAction($xml)
	{
		$this->logger->info("Atendiendo la peticion /factura/visualizarXml");
		return new ModelAndView('representacionGrafica', array($xml,$this->directoryTmp));
	}	
}
