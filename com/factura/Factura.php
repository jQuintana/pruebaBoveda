<?php
/**
 * Modelo de la clase Factura
 */
namespace com\factura;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Factura
 *
 * @author 		Israel Hern�ndez
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Factura
 * @version 	1.1
 *
 * @Component(name=Factura)
 * @Prototype
 */
class Factura extends BModel
{
	private $idFactura;
	private $idProveedor;
	private $idEmpresa;
	private $fechaAlta;
	private $serie;
	private $folio;
	private $uuid;
	private $fechaFactura;
	private $subtotal;
	private $iva;
	private $tasaIva;
	private $total;
	private $moneda;
	private $correoElectronico;
	private $estatus;
	private $fechaModificacion;
	private $usuarioModificacion;
	private $xml;
	private $ordenCompra;
	private $representacionGrafica;
	private $albaran;
	private $rfcProveedor;
	private $rfcEmpresa;
	private $noOrdenCompra;
	use TModel;

	public function getIdFactura()
	{
		return $this->idFactura ;
	}
	
	public function setIdFactura($idFactura)
	{
		$this->idFactura = $idFactura;
	}
	
	public function getIdProveedor()
	{
		return $this->idProveedor ;
	}
	
	public function setIdProveedor($idProveedor)
	{
		$this->idProveedor = $idProveedor;
	}
	
	public function getIdEmpresa()
	{
		return $this->idEmpresa ;
	}
	
	public function setIdEmpresa($idEmpresa)
	{
		$this->idEmpresa = $idEmpresa;
	}
	
	public function getFechaAlta()
	{
		return $this->fechaAlta ;
	}

	public function setFechaAlta($fechaAlta)
	{
		$this->fechaAlta = $fechaAlta;
	}
	
	public function getOrdenCompra(){
		return $this->ordenCompra ;
	}
	
	public function setOrdenCompra($ordenCompra)
	{
		$this->ordenCompra = $ordenCompra;
	}

	public function getNoOrdenCompra(){
		return $this->noOrdenCompra ;
	}
	
	public function setNoOrdenCompra($noOrdenCompra)
	{
		$this->noOrdenCompra = $noOrdenCompra;
	}
	
	public function getSerie()
	{
		return $this->serie ;
	}
	
	public function setSerie($serie)
	{
		$this->serie = $serie;
	}
	
	public function getFolio()
	{
		return $this->folio ;
	}
	
	public function setFolio($folio)
	{
		$this->folio = $folio;
	}
	
	public function getUuid()
	{
		return $this->uuid ;
	}
	
	public function setUuid($uuid)
	{
		$this->uuid = $uuid;
	}
	
	public function getFechaFactura()
	{
		return $this->fechaFactura ;
	}
	
	public function setFechaFactura($fechaFactura)
	{
		$this->fechaFactura = $fechaFactura;
	}
	
	public function getSubtotal()
	{
		return $this->subtotal ;
	}
	
	public function setSubtotal($subtotal)
	{
		$this->subtotal = $subtotal;
	}
	
	public function getIva()
	{
		return $this->iva ;
	}
	
	public function setIva($iva)
	{
		$this->iva = $iva;
	}
	
	public function getTasaIva()
	{
		return $this->tasaIva ;
	}
	
	public function setTasaIva($tasaIva)
	{
		$this->tasaIva = $tasaIva;
	}
	
	public function getTotal()
	{
		return $this->total ;
	}
	
	public function setTotal($total)
	{
		$this->total = $total;
	}
	
	public function getMoneda()
	{
		return $this->moneda ;
	}
	
	public function setMoneda($moneda)
	{
		$this->moneda = $moneda;
	}
	
	public function getCorreoElectronico()
	{
		return $this->correoElectronico ;
	}
	
	public function setCorreoElectronico($correoElectronico)
	{
		$this->correoElectronico = $correoElectronico;
	}
	
	public function getEstatus()
	{
		return $this->estatus;
	}
	
	public function setEstatus($estatus)
	{
		$this->estatus = $estatus;
	}
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion ;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	public function getUsuarioModificacion()
	{
		return $this->usuarioModificacion ;
	}
	
	public function setUsuarioModificacion($usuarioModificacion)
	{
		$this->usuarioModificacion = $usuarioModificacion;
	}
	
	public function getXml()
	{
		return $this->xml ;
	}
	
	public function setXml($xml)
	{
		$this->xml = $xml;
	}
	
	public function getRepresentacionGrafica()
	{
		return $this->representacionGrafica ;
	}
	
	public function setRepresentacionGrafica($representacionGrafica)
	{
		$this->representacionGrafica = $representacionGrafica;
	}
	
	public function getAlbaran()
	{
		return $this->albaran ;
	}
	
	public function setAlbaran($albaran)
	{
		$this->albaran = $albaran;
	}
	
	public function getRfcProveedor()
	{	
		return $this->rfcProveedor;
	}
	
	public function setRfcProveedor($rfcProveedor)
	{
		$this->rfcProveedor = $rfcProveedor;
	}
	
	public function getRfcEmpresa()
	{
		return $this->rfcEmpresa ;
	}
	
	public function setRfcEmpresa($rfcEmpresa)
	{
		$this->rfcEmpresa = $rfcEmpresa;
	}
}
