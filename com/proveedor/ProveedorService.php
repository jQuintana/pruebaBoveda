<?php
/**
 * Servicios del recurso Proveedor
 */
namespace com\proveedor;

use MNIComponents\Base\TService;


/**
 * Servicios del recurso Proveedor
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Bvoeda
 * @subpackage 	Proveedor
 * @version 	1.1
 * 
 * @Component(name=ProveedorService)
 * @Singleton
 */
class ProveedorService
{
	/** @Resource(name=ProveedorDao) */
	protected $proveedorDao;	
	/** @Resource(name=ACLService) */
	protected $aclService;
	/** @Resource(name=SesionService) */
	protected $sesionService;
	protected $logger;
	use TService;

	/**
	 * Inserta un nuevo proveedor
	 * @param \com\proveedr\Proveedor $proveedor
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function insertar($proveedor, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$response = $this->proveedorDao->insertar($proveedor);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Actualiza un proveedor
	 * @param \com\proveedr\Proveedor $proveedor
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function actualizar($proveedor, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$response = $this->proveedorDao->actualizar($proveedor);
			return  $this->sesionService->token($sesion);
		}
	}

	/**
	 * Elimina un proveedor
	 * @param \com\proveedr\Proveedor $proveedor
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function eliminar($proveedor, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->proveedorDao->consultar($proveedor);
			$proveedor->__fromArray($list[0]);
			$proveedor->setEstatus(2);
			$this->proveedorDao->actualizar($proveedor);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Obtiene un listado de proveedores
	 * @param \com\proveedr\Proveedor $proveedor
	 * @param \com\jqgridfilter\JQGridFilter $jqgridfilter
	 * @return array
	 */
	public function listar($proveedor)
	{
		return  $this->proveedorDao->listar($proveedor);
	}

	/**
	 * Consulta un proveedor dado
	 * @param \com\proveedr\Proveedor $proveedor
	 * @return array
	 */
	public function consultar($proveedor)
	{
		return $this->proveedorDao->consultar($proveedor);
	}
}
