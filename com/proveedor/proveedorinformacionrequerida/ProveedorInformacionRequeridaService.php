<?php
/**
 * Servicios del recurso ProveedorInformacionRequerida
 */
namespace com\proveedor;

use MNIComponents\Base\TService;


/**
 * Servicios del recurso ProveedorInformacionRequerida
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Component(name=ProveedorInformacionRequeridaService)
 * @Singleton
 */
class ProveedorInformacionRequeridaService
{
	/** @Resource(name=ProveedorInformacionRequerida) */
	protected $proveedorInformacionRequerida;
	/** @Resource(name=ProveedorInformacionRequeridaDao) */
	protected $proveedorInformacionRequeridaDao;
	/** @Resource(name=ProveedorService) */
	protected $proveedorService;
	/** @Resource(name=InformacionRequeridaService) */
	protected $informacionRequeridaService;
	/** @Resource(name=SesionService) */
	protected $sesionService;
	/** @Resource(name=ACLService) */
	protected $aclService;
	protected $logger;	
	use TService;

	/**
	 * Obtiene listado de la relacion proveedor/informacion requerida
	 * @param \com\proveedor\ListarProveedorInformacionRequerida $listarProveedorInformacionRequerida
	 * @return array
	 */
	public function listar($proveedorInformacionRequerida)
	{
		return $this->proveedorInformacionRequeridaDao->listar($proveedorInformacionRequerida);
	}

	/**
	 * Consulta la relacion proveedor/informacion requerida determinada de un proveedor dado
	 * @param \com\proveedor\ListarProveedorInformacionRequerida $listarProveedorInformacionRequerida
	 * @return array
	 */
	public function consultar($proveedor)
	{
		$list = $this->proveedorService->consultar($proveedor);
		if(!empty($list)){
			$proveedor->__fromArray($list[0]);
			$this->proveedorInformacionRequerida->setIdProveedor($proveedor->getidProveedor());
			return $this->proveedorInformacionRequeridaDao->consultar($this->proveedorInformacionRequerida);
		}
	}

	/**
	 * Inserta una relacion proveedor/informaicon requerida
	 * @param \com\proveedor\InformacionRequerida $proveedor
	 * @param \com\proveedor\Proveedor $listarProveedorInformacionRequerida
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function insertar($proveedor, $informacionRequerida, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->proveedorService->consultar($proveedor);
			$proveedor->__fromArray($list[0]);

			$this->proveedorInformacionRequerida->setIdInformacionRequerida($informacionRequerida->getIdInformacionRequerida());
			$this->proveedorInformacionRequerida->setIdProveedor($proveedor->getIdProveedor());

			$response = $this->proveedorInformacionRequeridaDao->insertar($this->proveedorInformacionRequerida);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Actualiza una relacion proveedor/informaicon requerida
	 * @param \com\proveedor\InformacionRequerida $proveedor
	 * @param \com\proveedor\Proveedor $listarProveedorInformacionRequerida
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function eliminar($proveedor, $informacionRequerida, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->proveedorService->consultar($proveedor);
			$proveedor->__fromArray($list[0]);

			$this->proveedorInformacionRequerida->setIdInformacionRequerida($informacionRequerida->getIdInformacionRequerida());
			$this->proveedorInformacionRequerida->setIdProveedor($proveedor->getIdProveedor());

			$response = $this->proveedorInformacionRequeridaDao->eliminar($this->proveedorInformacionRequerida);
			return $this->sesionService->token($sesion);
		}
	}
}
