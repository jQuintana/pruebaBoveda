<?php
/**
 * Servicios del recurso InformacionRequerida
 */
namespace com\proveedor\informacionrequerida;

use MNIComponents\Base\TService;


/**
 * Servicios del recurso InformacionRequerida
 * 
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 * 
 * @Component(name=InformacionRequeridaService)
 * @Singleton
 */
class InformacionRequeridaService
{
	/** @Resource(name=InformacionRequeridaDao) */
	protected $informacionRequeridaDao;
	protected $logger;	
	use TService;

	/**
	 * Obtiene un listado de informacion requeridas
	 * @param \com\proveedor\InformacionRequerida $InformacionRequerida
	 * @return array
	 */
	public function listarInformacionRequerida($InformacionRequerida)
	{
		return $this->informacionRequeridaDao->listar($InformacionRequerida);
	}

	/**
	 * Consulta iformacion requerida determinada
	 * @param \com\proveedor\InformacionRequerida $InformacionRequerida
	 * @return array
	 */
	public function consultarInformacionRequerida($InformacionRequerida)
	{
		return $this->informacionRequeridaDao->consultar($InformacionRequerida);
	}
}
