<?php
/**
 * Controlador para el recurso InformacionRequerida
 */
namespace com\proveedor\informacionrequerida;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/proveedor
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/informacionRequerida})
 */
class InformacionRequeridaController
{
	/** @Resource(name=InformacionRequeridaService) */
	protected $informacionRequerida;	
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso InformacionRequerida/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /informacionRequerida/listar");
		$response = $this->informacionRequeridaService->listar($this->informacionRequerida);
		$this->response($response);
	}
}
