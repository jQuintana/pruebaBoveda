<?php
/**
 * Controlador para el recurso Empresa
 */
namespace com\empresa;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso empresa, atiende a las peticiones que contengas /mvc/empresa
 *
 * @author 		Israel Hern�ndez
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Empresa
 * @version 	1.1
 *
 * @Controller
 * @RequestMapping(url={/empresa})
 * @Singleton
 */
class EmpresaController
{
	/** @Resource(name=Empresa) */
	protected $empresa;
	/** @Resource(name=EmpresaService) */
	protected $empresaService;
	/** @Resource(name=Sesion) */
	protected $sesion;
	protected $logger;	
	use TController;

	/**
	 * Atiende peticiones al recurso empresa/insertar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function insertarAction($empresa, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /empresa/insertar");
		$this->empresa->__fromJson($empresa);
		$this->sesion->__fromJson($sesion);
		$response = $this->empresaService->insertar($this->empresa, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso empresa/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /empresa/listar");
		$response = $this->empresaService->listar($this->empresa);
		$this->response($response);
	}
	
	/**
	 * Atiende peticiones al recurso empresa/consultar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function consultarAction($empresa)
	{
		$this->logger->info("Atendiendo la peticion /empresa/consultar");
		$this->empresa->__fromJson($empresa);
		$response = $this->empresaService->consultar($this->empresa);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso empresa/actualizar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function actualizarAction($empresa, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /empresa/actualizar");
		$this->empresa->__fromJson($empresa);
		$this->sesion->__fromJson($sesion);
		$response = $this->empresaService->actualizar($this->empresa, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso empresa/eliminar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function eliminarAction($empresa, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /empresa/eliminar");
		$this->empresa->__fromJson($empresa);
		$this->sesion->__fromJson($sesion);
		$response = $this->empresaService->eliminar($this->empresa, $this->sesion);
		$this->response($response);
	}
}
