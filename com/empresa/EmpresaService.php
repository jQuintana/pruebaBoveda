<?php
/**
 * Servicios del recurso empresa
 */
namespace com\empresa;

use MNIComponents\Base\TService;
use	com\sesion\Sesion;


/**
 * Servicios del recurso Empresa
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Empresa
 * @version 	1.1
 * 
 * @Component(name=EmpresaService)
 * @Singleton
 */
class EmpresaService
{
	/** @Resource(name=EmpresaDao) */
	protected $empresaDao;
	/** @Resource(name=SesionService) */
	protected $sesionService;
	/** @Resource(name=ACLService) */
	protected $aclService;
	protected $logger;	
	use TService;

	/**
	 * Servicio de insercion de empresas
	 * @param \com\empresa\Empresa $empresa
	 * @param \com\sesion\Sesion $sesion
	 * @return boolean
	 */
	public function insertar(Empresa $empresa, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->empresaDao->insertar($empresa);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Servicio para eliminar empresas
	 * @param \com\empresa\Empresa $empresa
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function eliminar($empresa, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->consultar($empresa);
			$empresa->__fromArray($list[0]);
			$empresa->setEstatus(2);
			$this->empresaDao->actualizar($empresa);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Servicio de actualizacion de datos de una empresa
	 * @param \com\empresa\Empresa $empresa
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function actualizar(Empresa $empresa, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->empresaDao->actualizar($empresa);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Obtiene un listado de empresas existentes
	 * @param Empresa $empresa
	 * @return array
	 */
	public function listar(Empresa $empresa)
	{
		return $this->empresaDao->listar($empresa);
	}

	/**
	 * Consulta una empres dada
	 * @param \com\empresa\Empresa $empresa
	 * @return array
	 */
	public function consultar(Empresa $empresa)
	{
		return $this->empresaDao->consultar($empresa);
	}
}
