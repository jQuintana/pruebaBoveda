/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de Provedores/informacion requerida
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Proveedor
 */
 define([],function(){

	/**
	 * Genera las opciones, aquella informacion requerida que uaun puede ser asignada a aun proveedor para la creacion
	 * de esta realcion en el catalogo
	 */
	function generaOpciones(proveedor){
		var opciones = "";
		var response = $.obtenJson("proveedorInformacionRequerida/listar", { proveedor: { idProveedor: proveedor } });
		for(var i = 0; i < response.length; i++)
			opciones+= response[i].idInformacionRequerida+":"+response[i].valor+";";
		return  opciones.substring(0,opciones.length-1);
	}

	/**
	 * Crea e inicializa los enventos que controla el grid de proveedores/infromacion requerida
	 */
	function init(idProveedor){
		/**
		 * inicializa las variables que controlan el grid
		 */
		var nameTable 	= "table_jsonProveedoresHasInformacionRequerida";
		var table 		= $("#"+nameTable);
		var pager2 		= "#div_pager2";

		/**
		 * Crea y configura el grid de la relacion proveedor e informacion requerida
		 */
		table.jqGrid({
			url: $.urlBase+'proveedorInformacionRequerida/consultar',
			datatype: "json",
			postData: { proveedor: $.toJSON({ idProveedor: idProveedor }) },
			colNames:['idProveedor','Proveedor', 'RFC', 'Informacion Requerida'],
			colModel:[
				{name:'idProveedor',index:'idProveedor', hidden:true},
				{name:'nombre',index:'nombre', hidden:true},
				{name:'rfc',index:'rfc', hidden:true},
				{name:'idInformacionRequerida',index:'idInformacionRequerida', key:true, editable:true, edittype:"select", editoptions: {value: generaOpciones(idProveedor), dataInit: function(elem){$(elem).css('width', "80%");}}, editrules:{required:true}}
			],
			pager: pager2,
			sortname: 'nombre',
			caption: "Proveedores Información Requerida "+idProveedor+" - Boveda"
		});

		/**
		 * Configura los enventos de insercion y eliminacion de relaciones entre proveedor e informaicon requerida
		 */
		table.jqGrid('navGrid',pager2,
			{edit:false, add:true, del:true, search: false, refresh:true },
			{},
			{
                url: $.urlBase+"proveedorInformacionRequerida/insertar",
                beforeShowForm: function(form){
                	$('#editmod'+nameTable).center();
                },
				onclickSubmit: function(rowid){
					var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
					return { sesion: $.getSesionJSON(), proveedor: $.toJSON({ idProveedor: idProveedor }), informacionRequerida: $.toJSON(dataForm) };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			},
			{
				url: $.urlBase+"proveedorInformacionRequerida/eliminar",
				beforeShowForm: function(form){
                	$('#delmod'+nameTable).center();
                },
                onclickSubmit:function(options, rowid){
					return { sesion: $.getSesionJSON(), proveedor: $.toJSON({ idProveedor: idProveedor }), informacionRequerida: $.toJSON({idInformacionRequerida: rowid}) };					
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			}
		);
	}

	return {
		init: init
	};
});
