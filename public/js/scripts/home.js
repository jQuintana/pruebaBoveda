/**
 * Módulo JavaScript, controla los eventos en la pantalla principal de la boveda
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage  Factura
 */
define([], function () {
	var listEmpresas;

	/**
	 * Genera la fecha actual en formato aaaa-mm-dd
	 */
	function obtenFecha(){
		var date = new Date();
		var anho = date.getUTCFullYear();
		var mes = date.getMonth()+1;
		var dia = date.getDate();
		mes = (mes < 10) ? "0"+mes : mes;
		dia = (dia < 10) ? "0"+dia : dia;

		return (anho+"-"+mes+"-"+dia);
	}

	/**
	 * Realiza la peticion validacion de un usuario, apartir de la respuesta setea el objeto localStorage
	 */
	function init(home_html, home_js, header_html, header_js, footer_html){
		var dataForm = $('#form').serializeFormJSON();
		$.obtenJson("usuario/login", {usuario:  dataForm}, function(response){
			if(response != false){
				$.setSesion(response[response.length-1]);
				$('header').hide().html(header_html).fadeIn();
				$('footer').hide().html(footer_html).fadeIn();
				$("#bari").text("boveda :: Administración de Facturas");
				$("#main").hide().html(home_html).fadeIn();
				//$.obtenJson('usuarioEmpresa/consultar', { usuarioEmpresa: { idUsuario: $.evalJSON($.getSesionJSON()).idUsuario } }, function (list) {
					//listEmpresas = list;
					home_js.table();
					header_js.init();
				//});		
			}
		});
		
	}

	/**
	 * Obtiene el objeto localStorage y consulta la sesion, una vez devuelta la nueva sesion actualiza el objeto
	 * localStorage
	 */
	function rememberMe(home_html, home_js, header_html, header_js, footer_html){
		var sesion = $.getSesionJSON();
		$.obtenJson("sesion/consultar", {sesion : $.evalJSON(sesion)}, function(sesion){
			$('header').hide().html(header_html).fadeIn();							
			$('footer').hide().html(footer_html).fadeIn();
			$("#bari").text("boveda :: Administración de Facturas");
			$("#main").hide().html(home_html).fadeIn();			
			//$.obtenJson('usuarioEmpresa/consultar', { usuarioEmpresa: { idUsuario: sesion.idUsuario } }, function (list) {				
				//listEmpresas = list;
				home_js.table();
				header_js.init();
				$.setSesion(sesion);
			//});
		});
	}

	/**
	 * Esta funcion se encarga de generar los links de la columna de opciones del grid
	 * @param cellvalue
	 * @param optiones
	 * @param rowObject Un objeto que ha llenado el la columa especifica del grid
	 * @return string el html generado para crear los links
	 */
	function linkFormatter(cellvalue, options, rowObject){
		var link = "";
		if(rowObject.xml != null && rowObject.xml != "")
			link+='<a href="#facturaXML_'+rowObject.xml+'" id="'+rowObject.xml+'"><span style="float:left" data-toggle="tooltip" class="ui-icon ui-icon-clipboard" title="XML"></span></a>';
		if(rowObject.representacionGrafica != null && rowObject.representacionGrafica != "")
			link+='<a href="#facturaRepresentacionGrafica_'+rowObject.representacionGrafica+'" id="'+rowObject.representacionGrafica+'"><span style="float:left" data-toggle="tooltip" class="ui-icon ui-icon-copy" title="Representación Grafica"></span></a>';
		if(rowObject.ordenCompra != null && rowObject.ordenCompra != "")
			link+='<a href="#facturaOrdenCompra_'+rowObject.ordenCompra+'" id="'+rowObject.ordenCompra+'"><span style="float:left" data-toggle="tooltip" class="ui-icon ui-icon-folder-open" title="Orden de Compra"></span></a>';
		if(rowObject.albaran != null && rowObject.albaran != "")
			link+='<a href="#facturaAlbara_'+rowObject.albaran+'" id="'+rowObject.albaran+'"><span style="float:left" data-toggle="tooltip" class="ui-icon ui-icon-document" title="Albaran"></span></a>';

		return link;
	}
	
	function getIdEmpresas() {
		var ids = [];
		for (var e in listEmpresas) { 
			ids.push(listEmpresas[e].idEmpresa);
		}
		return ids;
	}

	/**
	 * configuracion de los eventos que controlan el grid de la boveda
	 */
	function table() {	
		
		/**
		 * Inicializamos las variables para el manejo del grid y su paginador
		 */
		var nameTable 	= "table_jsonBoveda";
		var table 		= $("#"+nameTable);
		var pager		= "#div_pager";

		/**
		 * Controla el evento del boton de descargarArchivo, realiza una peticion que descarga un archivo
		 * zip con los xml's de todas las facturas
		 */
		$("#descargarArchivo").click(function(){
			var download = $("#preparing-file-modal");
			download.dialog({
				modal: true,
				show: { effect: "fade", duration: 500 },
				hide: { effect: "fade", duration: 500 },
				closeOnEscape: false,
				dialogClass: 'no-close',
				open: function(event, ui){
					$(".ui-dialog-titlebar-close",ui).hide();
					$(this).parent().children().children('.ui-dialog-titlebar-close').hide();
				}
			});

			$.fileDownload('mvc/descarga/descargarArchivos', {
				httpMethod: "POST",
				data: { idEmpresas: $.toJSON(getIdEmpresas()) },				
				successCallback: function(url) {
					download.dialog('close');
				},
				failCallback: function(responseHtml, url) {
					download.dialog('close');
					$("#error-modal").dialog({ modal: true });
				}
			});
			return false;
		});

		/**
		 * Controla el evento del boton de descargarXml, realiza una peticion que descarga un archivo
		 * zip con los xml's que ha sido checkeados en el grid
		 */
		$("#descargarXml").click(function(){
			var selectedIDs = table.jqGrid('getGridParam', 'selarrrow');
			if(selectedIDs.length == 0)
				return false;

			var param = "";
			for(var i = 0; i < selectedIDs.length; i++){
				param+= selectedIDs[i]+",";
			}
			param = param.substring(0, param.length-1);

			var download = $("#preparing-file-modal");
			download.dialog({
				modal: true,
				show: { effect: "fade", duration: 500 },
				hide: { effect: "fade", duration: 500 },
				closeOnEscape: false,
				dialogClass: 'no-close',
				open: function(event, ui){
					$(".ui-dialog-titlebar-close",ui).hide();
					$(this).parent().children().children('.ui-dialog-titlebar-close').hide();
				}
			});

			$.fileDownload('mvc/descarga/descargar' ,{
				httpMethod: "POST",
        		data: { cfdis: param },
				successCallback: function(url) {
					download.dialog('close');
				},
				failCallback: function(responseHtml, url) {
					download.dialog('close');
					$("#error-modal").dialog({ modal: true });
				}
			});
			return false;
		});

		/**
		 * Crea el evento que abre los archivos referenciados en el grid en un popup centrado en la pantalla
		 */
		table.delegate("a", "click", function(){
			var id = $(this).attr("id");
			var w = 550;
			var h = 450;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/2);
			window.open(id, '', 'toolbar=no,location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		});

		/**
		 * Creacion y configuracion del grid de boveda
		 */
		 $.obtenJson('usuarioEmpresa/consultar', { usuarioEmpresa: { idUsuario: $.evalJSON($.getSesionJSON()).idUsuario } }, function (list) {
			listEmpresas = list;		
			
			var mygrid = table.jqGrid({
				url: $.urlBase + 'factura/listarEn',
				postData: { idEmpresas: $.toJSON(getIdEmpresas()) },			
				datatype: "json",
				colNames:['ID', 'Fecha Alta', 'Receptor','Receptor', 'Emisor', 'ID Prov.','Serie', 'Folio', 'UUID', '# Orden Compra', 'Fecha Factura', 'IVA', 'Tasa', 'Total', 'Moneda', 'Correo Electrónico', 'Estatus', 'Fecha Modificación', 'Modificador', 'Opciones', 'xml', 'representacionGrafica', 'albaran', 'ordenCompra' ],
				colModel:[
					{name:'idFactura', index:'idFactura', key:true, width:80, editable:true},
					{name:'fechaAlta', index:'fechaAlta', searchoptions:{sopt:['eq','ne'], dataInit:function(elem){$(elem).datepicker({onSelect: function(){if(this.id.substr(0, 3) === "gs_"){setTimeout(function(){mygrid[0].triggerToolbar();}, 50);}else $(this).trigger('change');}});}}},
					{name:'idEmpresa', index:'idEmpresa', hidden:true, search:false},
					{name:'rfcEmpresa', index:'rfcEmpresa'},
					{name:'rfcProveedor', index:'rfcProveedor'},
					{name:'idProveedor', index:'idProveedor'},
					{name:'serie', index:'serie', editable:true},
					{name:'folio', index:'folio', editable:true},
					{name:'uuid', index:'uuid', editable:true},
					{name:'noOrdenCompra', index:'noOrdenCompra', editable:true},
					{name:'fechaFactura', index:'fechaFactura', editable:true, searchoptions:{sopt:['eq','ne'], dataInit:function(elem){$(elem).datepicker({onSelect: function(){if(this.id.substr(0, 3) === "gs_"){setTimeout(function(){mygrid[0].triggerToolbar();}, 50);}else $(this).trigger('change');}});}}},
					{name:'tasaIva', index:'tasaIVa', hidden:true, search:false},
					{name:'iva',index:'iva', hidden:true, search:false},
					{name:'total',index:'total', editable:true, search:false, align:"right", width:100, formatter:'currency', formatoptions:{prefix:"$ ", thousandsSeparator:","}},
					{name:'moneda',index:'moneda', editable:true, align:"right", width:80},
					{name:'correoElectronico',index:'correoElectronico', editable:true},
					{name:'estatus',index:'estatus', align:"center", width:100, editable:true, edittype:"select", editoptions:{value:"Pendiente:Pendiente;Rechazada:Rechazada;Aceptada:Aceptada", dataInit: function (elem) {$(elem).css('width', "80%");}}, stype:'select', searchoptions:{sopt:['eq','ne'],value:':Todos;Rechazada:Rechazada;Aceptada:Aceptada;Pendiente:Pendiente'}},
					{name:'fechaModificacion',index:'fechaModificacion', hidden:true, search:false, editable:true},
					{name:'usuarioModificacion',index:'usuarioModificacion'},
					{name:'idFactura',index:'idFactura', formatter:linkFormatter, search:false},				
					{name:'xml',index:'xml', hidden:true, search:false, editable:true},
					{name:'representacionGrafica',index:'representacionGrafica', hidden:true, search:false, editable:true},
					{name:'albaran',index:'albaran', hidden:true, search:false, editable:true},
					{name:'ordenCompra',index:'ordenCompra', hidden:true, search:false, editable:true}
				],
				pager: pager,
				sortorder: "desc",
				multiselect: true,			
				multiboxonly: true,
				sortname: 'idFactura',
				caption: "Boveda"
			});
	
			/**
			 * Configuracion del paginador, define los eventos que realiza las accione de edicion, si es rechazada,
			 * mostrara un dialogo para realizar un envio de correo electornico al proveedor
			 */
			table.jqGrid('navGrid',pager,
				{edit:true, add:false, del:false, search: false, refresh:true, view:true},
				{
	                url: $.urlBase+"factura/actualizar",
					beforeShowForm: function(form){
						$('#editmod'+nameTable).center();
	                    $('#tr_idFactura', form).hide();
						$('#tr_correoElectronico', form).hide();
						$('#tr_fechaFactura', form).hide();
						$('#tr_total', form).hide();
						$('#tr_moneda', form).hide();
						$('#tr_usuarioModificacion', form).hide();					
						$('#tr_serie', form).hide();	
						$('#tr_folio', form).hide();	
						$('#tr_uuid', form).hide();	
						$('#tr_noOrdenCompra', form).hide();
						$("#total").val($("#total").val()/100);
						$("#usuarioModificacion").val($.localStorage.getItem("usuario"));
						$("#fechaModificacion").val(obtenFecha());
	                },
					onclickSubmit:function(rowid){
						var dataForm = $("#FrmGrid_table_jsonBoveda").serializeFormJSON();
						var request = new Object();
						request.factura = $.toJSON(dataForm);
	
						var aux = $.evalJSON(request.factura);
						if(aux.estatus == "Rechazada"){
							require(['req/text!html/enviaComentario.html','script/enviaComentario'],function(enviaComentario_html, enviaComentario_js){
	
								$(function() {
									$(enviaComentario_html).dialog({
										autoOpen: true,
										resizable: true,
										title: "Enviar Comentarios",
										modal: true,
										width: "auto",
										show: { effect: "fade", duration: 500 },
										hide: { effect: "fade", duration: 500 },
										buttons: {
											Enviar: function() {
												enviaComentario_js.enviar();
											}
										},
										closeOnEscape: false,
										dialogClass: 'no-close',
										open: function(event, ui){
											$(".ui-dialog-titlebar-close",ui).hide();
											$(this).parent().children().children('.ui-dialog-titlebar-close').hide();
										}
									});
								});
								enviaComentario_js.init(dataForm);
							});
	
						}
						return request;
					}
	            },{},{},{},{
	            	beforeShowForm: function(form){
						$('#viewmod'+nameTable).center();
					}
	            }
			);
	
			/**
			 * Configuracion del paginador, se agregan el boton que permiten manipular el searchtoolbar
			 */
			table.jqGrid('navButtonAdd', pager,{caption:"",title:"Ocultar/Mostrar barra de búsqueda", buttonicon :'ui-icon-pin-s',
				onClickButton:function(){
					mygrid[0].toggleToolbar()
				}
			});
	
			/**
			 * Configuracion del paginador, se agregan el boton que permiten manipular el searchtoolbar
			 */
			table.jqGrid('navButtonAdd', pager,{caption:"",title:"Limpiar Búsqueda",buttonicon :'ui-icon-refresh',
				onClickButton:function(){
					mygrid[0].clearToolbar()
				}
			});
	
			/**
			 * Configuracion del paginador, se agregan el boton que realiza una peticion para generar el reporte,
			 * descargando un archivo excel
			 */
			table.jqGrid('navButtonAdd', pager,{caption:"",title:"Excel Export",buttonicon :'ui-icon-calculator',
				onClickButton:function(){
					var filtro = new Object();
					$("input", $(".ui-search-toolbar")).each(function(){
						filtro[$(this).attr("id").replace(/gs_/g, "")] = ($(this).val() == "") ? null : $(this).val() ;
					});
					filtro.idEmpresas = $.toJSON(getIdEmpresas());	
					console.log(filtro);			
					
					var download = $("#preparing-file-modal");
					download.dialog({
						modal: true,
						show: { effect: "fade", duration: 500 },
						hide: { effect: "fade", duration: 500 },
						closeOnEscape: false,
						dialogClass: 'no-close',
						open: function(event, ui){
							$(".ui-dialog-titlebar-close",ui).hide();
							$(this).parent().children().children('.ui-dialog-titlebar-close').hide();
						}
					});
					
					$.fileDownload('mvc/reporte/descargar', {
						httpMethod: "POST",
	        			data: filtro,
						successCallback: function(url) {
							download.dialog('close');
						},
						failCallback: function(responseHtml, url) {
							download.dialog('close');
							$("#error-modal").dialog({ modal: true });
						}
					});				
					return false;
				}
			});
	
			/**
			 * Configuracion del filterToolbar para el grid
			 */
			table.jqGrid('filterToolbar');
		});
	}

	return {
		init: init,
		table: table,
		rememberMe: rememberMe
	};
});

