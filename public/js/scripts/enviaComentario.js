/**
 * M�dulo JavaScript, con las funcionalidade para envio del correo
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Factura
 */
define([],function(){

	/**
	 * Llenamos los campos de to y subject para enviar el correo
	 * @param dataForm un objeto con la informacion del formulario creado por el grid al editar un registro
	 */
	function init(dataForm){
		$("#to").val(dataForm.correoElectronico);
		$("#subject").val(
			"Se ha rechazado la "+
			" factura: " + dataForm.idFactura +
			", Serie: " + dataForm.serie +
			", Folio: " + dataForm.folio +
			", UUID: " + dataForm.uuid + "."
		);
	}

	/**
	 * Esta funcion realiza la serializacion del formulario para realizar la peticion de envio de correo
	 */
	function enviar(){
		if($("#text").val() != ""){
			var dataForm = $('#comentarioForm').serializeFormJSON();
			$.obtenJson("mail/enviar", { to: dataForm }, function(response){
				$("#div_comentarios").remove();	
			});			
		}else
			$("#text").focus();
	}

	return {
		init: init,
		enviar: enviar
	};
});
