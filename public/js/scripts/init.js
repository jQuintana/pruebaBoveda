/**
 * Módulo JavaScript, permite inicializar algunas funciones globales
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 */
define(function(){

	function init(config){
		/**
		 * Variables de uso global, urlBase y localStorage
		 */
		jQuery.urlBase 		= config.urlBase;
		jQuery.localStorage = localStorage;

		/**
		 * A partir del objeto localStorage crea un objeto sesion con los atributos necesarios como sesion, toke
		 * y usuario en formatio JSON
		 */
		jQuery.getSesionJSON = function(){
			var sesion 			= new Object();
			sesion.sesion		= 0;
			sesion.token 		= $.localStorage.getItem("token");
			sesion.idUsuario 	= $.localStorage.getItem("usuario");
			return $.toJSON(sesion);
		}

		/**
		 * A partir de un objeto sesion setea los valores a el objeto localStorage
		 */
		jQuery.setSesion = function(sesion){
			$.localStorage.setItem("token", sesion.token);
			$.localStorage.setItem("usuario", sesion.idUsuario);
		}

		/**
		 * Configuracion Regional de los calendarios en la pantalla.
		 */
		jQuery(function($){
			$.datepicker.regional['es'] = {
				closeText		: 'Cerrar',
				prevText		: '<Ant',
				nextText		: 'Sig>',
				currentText		: 'Hoy',
				monthNames		: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort	: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
				dayNames		: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sabado'],
				dayNamesShort	: ['Dom','Lun','Mar','Mié','Juv','Vie','Sab'],
				dayNamesMin		: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
				weekHeader		: 'Sm',
				dateFormat		: "yy-mm-dd",
				changeMonth		: true,
				changeYear		: true,
				maxDate			: new Date(),
				buttonImageOnly	: true,
				constrainInput	: true,
				firstDay		: 1,
				showWeek		: true,
				isRTL			: false
			};
			$.datepicker.setDefaults($.datepicker.regional['es']);
		});

		/**
		 * Si existe el objeto $.jqgrid (si es cargada la libreria jqgrid) configura los valores default de los
		 * grid para el resto de la aplicacion
		 *
		 * @viewrecords	muestra en la parte inferior derecha del grid el total de registros
		 * @sortorder define si el ordenamiento sera "asc" para ascendente o "desc" para descendiente
		 * @height define la altura del grid
		 * @autowidth define si la tabla se extenderá dentro del contenedor donde se encuentra el grid
		 * @rowNum define el número registros a mostrar
		 * @rowList permite determinar la variacion de registros a mostrar
		 * @emptyrecords define mensaje a mostrar en caso de no encotrar registros
		 * @rownumbers muestra una columna con los registros enumerados
		 * @gridview
		 * @mtype define el método de peticion de datos, GET o POST
		 * @jsonReader define un método para realizar lazyloads
		 * @loadError permite definir un método a realizar en caso de errores en el servidor
		 */
		if($.jgrid != null){
			$.extend($.jgrid.defaults,{
				ajaxGridOptions	: { async: true, timeout: 30000},
				ajaxCellOptions	: { async: true, timeout: 30000},
				ajaxRowOptions	: { async: true, timeout: 30000},
				viewrecords		: true,
				sortorder		: "asc",				
				height			: "auto",
				autowidth		: true,
				rowNum			: 10,
				rowList			: [10,15,20],
				emptyrecords	: "No hay registros",
				rownumbers		: true,
				gridview		: true,
				mtype			: "POST",
				loadtext		: 'Cargando...',
				beforeRequest	: function(){
									$.blockUI({
										message		: '<h5>Espere un momento...</h5>',
										theme		: true,
										fadeIn		: 0 
									});
								},
				jsonReader		: {
									repeatitems	: true,
									cell		: "",
									id			: "0"
								},
				loadError 		: function(xhr, st, err) {									
									$.showErrorDialog(st, xhr, err);									
								}
			});

			/**
			 * Configura los valores default de la edicion/insercion/eliminacion para los grid para el resto
			 * de la aplicacion
			 *
			 * @recreateForm generará un formulario a partir de la informacion en el grid
			 * @modal al mostrar el formulario genera un modal
			 * @closeOnEscape permite cerrar el formulario presionando "esc"
			 * @height define la altura del contenedor del formulario
			 * @width define el ancho del contenedor del formulario
			 * @processData	define el mensaje a mostrar mientras se procesa la informacion
			 * @closeAfterEdit define si al terminar la edicion se cerrará el formulario
             * @mtype define el metodo de peticion de los datos GET o POST
			 * @closeAfterAdd define si al terminar la insercion se cerrará el formulario
			 * @reloadAfterSubmit define si al terminar la peticion se recargara el grid
			 */
			$.extend($.jgrid.edit,{
				recreateForm		: true,
				modal				: true,
				closeOnEscape		: true,
				height				: "auto",
				width				: "auto",
				processData			: "Procesando...",
				closeAfterEdit 		: true,
                mtype				: "POST",
				closeAfterAdd		: true,
				reloadAfterSubmit	: true,
				errorTextFormat		: function(data){return "";}
			});
			$.extend($.jgrid.add,{
				recreateForm		: true,
				modal				: true,
				closeOnEscape		: true,
				height				: "auto",
				width				: "auto",
				processData			: "Procesando...",
				closeAfterEdit 		: true,
                mtype				: "POST",
				closeAfterAdd		: true,
				reloadAfterSubmit	: true,
				errorTextFormat		: function(data){return "";}
			});
			$.extend($.jgrid.del,{
				recreateForm		: true,
				modal				: true,
				closeOnEscape		: true,
				height				: "auto",
				width				: "auto",
				processData			: "Procesando...",
				closeAfterEdit 		: true,
                mtype				: "POST",
				closeAfterAdd		: true,
				reloadAfterSubmit	: true,
				errorTextFormat		: function(data){return "";}
			});
			$.extend($.jgrid.view,{
				recreateForm		: true,
				modal				: true,
				closeOnEscape		: true,
				height				: "auto",
				processData			: "Procesando..."
			});
		}

		/**
		 * Configuración Default del Objeto AJAX de JQuery
		 *
		 * @url Indicamos la ruta al servidor que se encarga de atender las peticiones
		 * @timeout Indicamos el tiempo de espera de respuesta del servidor
		 * @async Indicamos el modo de peticion será sincrono o asincrono
		 * @cache Indicamos si guardaremos en la cache las respuestas del servidor
		 * @type Indicamos por que metodo se enviará la información al servidor		  
		 * @contentType Indicamos el tipo de codificación de los datos enviados
		 */
		$.ajaxSetup({
			url:			jQuery.urlBase,
			timeout:		15000,
			async:			false,
			cache:			false,
			type:			'post',
			dataType:		'json',			
			contentType:	'application/x-www-form-urlencoded; charset=utf-8'
		});

		/**
		 * Configuración Default del Objeto AJAX al recibir un error
		 */
		$(document).ajaxError(function(xhr, textStatus, thrownError){
			$.showErrorDialog(textStatus, xhr, thrownError);
		});

		/**
		 * Configuración Default del Objeto AJAX al completar una peticion
		 */
		$(document).ajaxComplete(function(){
			$('body').delay(500).fadeIn(0, function(){$.unblockUI()});
		});
		
		/**
		 * Configuración Default del Objeto AJAX al completar una peticion
		 */
		$(document).ajaxStop(function(){		
			$('body').delay(500).fadeIn(0, function(){$.unblockUI()});
		});

		/**
		 * Configuracion basica para mostrar los errores o timeout que devuelve el servidor por AJAX
		 */
	    jQuery.showErrorDialog = function(xhr, textStatus, thrownError){
			var msg = "";
	        if(xhr.statusText === "timeout")
				msg = 'Mensaje: Lo sentimos, se ha detectado un problema con la comunicación por lo que no se ha podido completar la operación solicitada. Por favor verifique su conexión o intente más tarde.';
			else{
				try{
					var obj = $.evalJSON(xhr.responseText);					
					msg = 'Mensaje: <b><i>'+obj.message+'</i></b><br/> Fecha: <b><i>'+obj.time+'</i></b><br/> Código de Error : <b><i>'+obj.code+'</i></b>';
				}catch(e){
					console.log(xhr.responseText);
					return false;
				}
			}
			
	        $('<div class="row">' +
	        	'<div class="span4">' +
	            	'<p style="text-align:justify;"><span class="icon-warning-sign" ></span> '+msg+'</p>'+
	            	'</div>'+
	            '</div>').dialog({
	            resizable		: false,
	            modal			: true,	            
	            width 			: "auto",
				title			: 'Error',
	            buttons			: { "Cerrar": function(){ $(this).dialog("close"); } },
	            open			: function(event, ui){$(".ui-widget-overlay").css({ "z-index": 1030});},
				close			: function(event, ui){ $(this).remove(); }
	        });
	    };
		
		/**
		 * Funcion para bloquear la pantalla
		 */
		function blockUI(){						
			$.blockUI({
				message		: '<h5>Espere un momento...</h5>',
				theme		: true,
				fadeIn		: 0 
			});
		}

		/**
		 * Realiza una llamada AJAX al servidor, la peticion apunta a una url de un recurso, enviando un grupo
		 * de objetos mediante una peticion POST serializadon como JSON, esperando obtener como respuesta un JSON
		 * desde el servidor
		 *
		 * @param string resource
		 * @param array objectsArray
		 * @return json
		 */
		jQuery.obtenJson = function(resource, data, callback, unblock){
			if(unblock == undefined)
				blockUI();		

			var async = (callback != undefined) ? true : false;
			var response = false;

			for(var prop in data){
				data[prop] = $.toJSON(data[prop]);
			}
			
			$.ajax({
				url			: $.urlBase+resource,
				data		: data,
				async		: async,				
				success		: function(json){
								response = json;
								if(callback != undefined) callback(response);								
							  },
				error 		: function(e){ console.log(e); }
			});			
			return response;
		}

		/**
		 * Realiza una llamada AJAX al servidor, la peticion apunta a una url de un recurso, enviando un grupo
		 * de objetos mediante una peticion POST serializadon como JSON, esperando obtener como respuesta un JSON
		 * desde el servidor
		 *
		 * @param string resource
		 * @param array objectsArray
		 * @return json
		 *
		jQuery.obtenJson = function(resource, objectsArray, callback){
			blockUI();		

			var async = (callback != undefined) ? true : false;
			var response = false;
			var data = new Object();
			for(var i = 0; i < objectsArray.length; i++){
				var Objeto = $.evalJSON(objectsArray[i]);
				var name = "";
				for(var prop in Objeto){
					name = prop
					break;
				}
				data[name] = objectsArray[i];
			}
			$.ajax({
				url			: jQuery.urlBase+resource,
				data		: data,
				async		: async,				
				success		: function(json){
								response = json;
								if(callback != undefined)
									callback(response);								
							}
			});
			return response;
		}*/
		/**
		 * Realiza una llamada AJAX al servidor permitiendo subir archivos
		 *
		 * @param resource la url del recurso al cual se hara la peticions
		 * @param data el objeto dataform
		 * @return json devuelve la respuesta del Servidor en formato JSON
		 */
		jQuery.uploadFile = function(resource, data, callback){	
			blockUI();								
			
			var async = (callback != undefined) ? true : false;			
			var response = false;
			$.ajax({
				url			: jQuery.urlBase+resource,
				data		: data,
				enctype		: 'multipart/form-data',
				cache		: false,
                contentType	: false,
                processData	: false,
				timeout		: 30000,
				async		: async,
				success		: function(json){
								response = json;
								if(callback != undefined)
									callback(response);								
							}				
			});
			return response;
		}

		/**
		 * Valida acciones al presionar algunas teclas especificas
		 *
		 *	@kecode 13 enter
		 *	@kecode 8 backspace
		 *	@kecode 116 F5
		 */
		$('html').keydown(function(event){
			var input = $(document.activeElement).is("input:focus");
			var textarea = $(document.activeElement).is("textarea:focus");

			if((event.keyCode === 8 || event.keyCode === 116) && !input && !textarea){
				$('<div id="dialog-confirm" title="Atenci&oacute;n" class="row">' +
			       	'<div class="span4"><p style="text-align:justify;"><span class="icon-warning-sign" /> '+
			       	'Está a punto de salir de la aplicación, se perderan sus datos. Si desea permanecer en la página de clic en "Cancelar". Si desea salir de clic en "Aceptar".</p>' +
			       	'</div></div>' ).dialog({
			        resizable	: false,
			        modal		: true,
			        width 		: "auto",
			        buttons		: {
									Aceptar: function(){ $(this).dialog("close"); (event.keyCode === 8) ? history.back(-1) : location.reload(); },
									Cancelar: function(){ $(this).dialog("close"); }
								  },
					open		: function(event, ui){$(".ui-widget-overlay").css({ "z-index": 1030});},
					close		: function(event, ui){ $(this).remove(); }
			       });			    
				event.preventDefault();
			}
		});

	    /**
	     * Permite serializar todos los datos de un formulario
	     */
	    (function($){
	    	$.fn.serializeFormJSON = function(){
				var o = {};
				var a = this.serializeArray();
				$.each(a,function(){
					if(o[this.name]){
						if(!o[this.name].push){
							o[this.name] = [o[this.name]];
						}
						o[this.name].push(this.value || '');
					}else{
						o[this.name] = this.value || '';
					}
				});
				return o;
	    	};
	    })(jQuery);

		/**
		 * funcion que permite compatibilidad al usar ciertas funciones permitidas en ciertos navegadores
		 * o ciertas versiones de los mismos
		 */
		(function(){
			var method;
			var noop = function () {};
			var methods = [
				'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
				'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
				'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
				'timeStamp', 'trace', 'warn'
			];
			var length = methods.length;
			var console = (window.console = window.console || {});

			while (length--) {
				method = methods[length];
				// Only stub undefined methods.
				if (!console[method]) {
					console[method] = noop;
				}
			}
		}());

		/**
		 * Emulate FormData for some browsers
		 * MIT License
		 * (c) 2010 François de Metz
		 */
		(function(w){
			if (w.FormData)
				return;
			function FormData() {
				this.fake = true;
				this.boundary = "--------FormData" + Math.random();
				this._fields = [];
			}
			FormData.prototype.append = function(key, value) {
				this._fields.push([key, value]);
			}
			FormData.prototype.toString = function() {
				var boundary = this.boundary;
				var body = "";
				this._fields.forEach(function(field) {
					body+= "--" + boundary + "\r\n";
					// file upload
					if (field[1].name) {
						var file = field[1];
						body+= "Content-Disposition: form-data; name=\""+ field[0] +"\"; filename=\""+ file.name +"\"\r\n";
						body+= "Content-Type: "+ file.type +"\r\n\r\n";
						body+= file.getAsBinary() + "\r\n";
					} else {
						body+= "Content-Disposition: form-data; name=\""+ field[0] +"\";\r\n\r\n";
						body+= field[1] + "\r\n";
					}
				});
				body+= "--" + boundary +"--";
				return body;
			}
			w.FormData = FormData;
		})(window);

		/**
		 * Funcion Global, permite centrar un div en pantalla
		 */
		jQuery.fn.center = function(){
			this.css("position","absolute");
			this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
			this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
			return this;
		}

		/**
		 * Funcion Global, permite obtener el valor de un parametro enviado por URL
		 *
		 * @param string Nombre de la variable a obtener
		 * @return string El valor del parametro si existe este, de lo contrario devuelve 0
		 */
		$._GET = function(name){
			var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(top.window.location.href);
			return (results !== null) ? results[1] : 0;
		}
	};

    return init;
});