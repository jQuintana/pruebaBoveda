/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de roles del usuario
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Usuario
 */
define([],function(){

	/**
	 * Genera obciones apartir de un usuario, es decir trae aquellos roles que un usuario no tiene y genera una cadena
	 * que se utiliza para configurar el value del select de roles para el usuario
	 */
	function generaOpciones(usuarioValue){
		var opciones = "";
		var response = $.obtenJson("usuarioRol/listar", { usuario: { usuario: usuarioValue }});
		for(var i = 0; i < response.length; i++)
			opciones+= response[i].idRol+":"+response[i].idRol+";";
		return  opciones.substring(0,opciones.length-1);
	}

	/**
	 * Configuran los eventos que controlaran el grid de usuario/rol
	 */
	function init(usuarioValue){
		/**
		 * Inicializamos variables para controlar el grid
		 */
		var nameTable 	= "table_jsonUsuariosHasRol";
		var table 		= $("#"+nameTable);
		var pager2 		= "#div_pager2";

		/**
		 * Se crea y configura el grid de los roles del usuario, agregando al usuario como valos extra en la peticion
		 */
		table.jqGrid({
			url: $.urlBase+'usuarioRol/consultar',
			datatype: "json",
			postData: { usuario: $.toJSON({ usuario: usuarioValue })},
			colNames:['usuario', 'Rol', 'Descripcion'],
			colModel:[
				{name:'usuario',index:'usuario', hidden:true},
				{name:'idRol',index:'idRol', key:true, editable:true, edittype:"select", editoptions: {value: generaOpciones(usuarioValue), dataInit: function (elem) {$(elem).css('width', "80%");}}},
				{name:'descripcion',index:'descripcion'}
			],
			pager: pager2,
			sortname: 'nombre',
			caption: "Usuario Roles "+usuarioValue+" Boveda"
		});

		/**
		 * Configuramos el paginador y los evneots de insercion, y eliminacion de roles para los usuarios
		 */
		table.jqGrid('navGrid', pager2,
			{edit:false, add:true, del:true, search: false, refresh:true },
			{},
			{
				url: $.urlBase+"usuarioRol/insertar",
				beforeShowForm: function(form){
                	$('#editmod'+nameTable).center();
                },
                onclickSubmit: function(rowid){
					var dataForm = $("#FrmGrid_table_jsonUsuariosHasRol").serializeFormJSON();
					return { rol: $.toJSON(dataForm), usuario: $.toJSON({ usuario: usuarioValue }), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			},
			{
                url: $.urlBase+"usuarioRol/eliminar",
                beforeShowForm: function(form){
                	$('#delmod'+nameTable).center();
                },
				onclickSubmit: function(options, rowid){					
					return { rol: $.toJSON({ idRol: rowid }), usuario: $.toJSON({ usuario: usuarioValue }), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			}
		);
	}

	return {
		init: init
	};
});
