/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de roles del usuario
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Usuario
 */
define([], function () {
	var contexto;
		
	var UsuarioEmpresa = function (idUsuario, idEmpresa) {
		if (idUsuario === undefined)
			idUsuario = null;
		if (idEmpresa === undefined)
			idEmpresa = null;
			
		this.idUsuario = idUsuario;
		this.idEmpresa = idEmpresa;
	};

	/**
	 * Configuran los eventos que controlaran el grid de usuario/rol
	 */
	function init(usuarioValue) {
		contexto = $("#div_usuarioEmpresas");
		
		$.obtenJson("empresa/consultar", { empresa : {estatus : 1} }, function (list) {
			var table = '<table class="table">';
			for (var empresa in list) { 
				table += '<tr>' +
					'<td><input type="checkbox" id="' + list[empresa].idEmpresa + '" value="' + list[empresa].idEmpresa + '" /></td>' +
					'<td>'+list[empresa].nombre+'</td>' +
				'</tr>';	
			}
			table+= '</table>';
			contexto.append(table);
						
			$.obtenJson("usuarioEmpresa/consultar", { usuarioEmpresa: new UsuarioEmpresa(usuarioValue, null) }, function (list2) {
				for (var empresa in list2) {
					$("#"+list2[empresa].idEmpresa, contexto).attr('checked', true);					
				}
			});		
			
			contexto.delegate("input:checkbox", 'click', function (evnet) {
				var idEmpresa = $(this).val();
				if ($(this).is(':checked')) {
					$.obtenJson("usuarioEmpresa/insertar", { usuarioEmpresa: new UsuarioEmpresa(usuarioValue, idEmpresa), sesion: $.evalJSON($.getSesionJSON()) }, function (response) {
						$.setSesion(response);
					}, false);					
				} else {
					$.obtenJson("usuarioEmpresa/eliminar", { usuarioEmpresa: new UsuarioEmpresa(usuarioValue, idEmpresa), sesion: $.evalJSON($.getSesionJSON()) }, function (response) {
						$.setSesion(response);
					}, false);					
				}										
			});
		});
	}
	return { init: init };	
});
