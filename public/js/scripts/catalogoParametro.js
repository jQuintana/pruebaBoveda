/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de Parámetros del Sistema
 *
 * @author 		Israel Hernández Valle
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Parametro
 */
define([],function(){

	/**
	 * Esta Funcion permite formatear un valor proveniente de grid
	 * @param string cellvalue
	 * @param Object options
	 * @param Object rowObject
	 * @return string
	 */
	function estatusFormatter(cellvalue, options, rowObject){
		return (cellvalue == 1) ? "Sí" : "No";
	}

	/**
	 * Funciona que inicializa el grid que carga los parametros del sistema, esta configuracion se encarga de las peticiones
	 * para la carga asi como para la edicion de datos
	 */
	function init(){
		/**
		 * Inicializamos la variables que controlan el grid
		 */
		var nameTable 	= "table_jsonParametros";
		var table 		= $("#"+nameTable);
		var pager 		= "#div_pager";

		/**
		 * Creamos y configuamos el grid de parametros
		 */
		table.jqGrid({
			url			: $.urlBase+'parametro/listar',
			editurl		: $.urlBase+'parametro/actualizar',
			datatype	: "json",
			colNames	: ['Código','Descripción', 'Valor', 'Unidad de medida', 'Notificación','Modificado','Modificador'],
			colModel	: [
							{name:'parametro',index:'parametro', editable:true, editrules:{required:true}},
							{name:'descripcion',index:'descripcion', width:250, editable:true, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
							{name:'valor',index:'valor', editable:true, align:"center", editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true, integer:true}},
							{name:'unidadMedida',index:'unidadMedida', align:"center"},
							{name:'notificacion',index:'notificacion', align:"center", formatter:estatusFormatter, editable:true, edittype:"select", editoptions:{value:"1:Sí;0:No", dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
							{name:'horaModificacion',index:'horaModificacion', align:"center"},
							{name:'usuarioModificacion',index:'usuarioModificacion', align:"center"}
						],
			pager		: pager,
			sortname	: 'parametro',
			caption		: "Parámetros - Boveda"
		});

		/**
		 * Configuramops el evento de edicion de registros del grid
		 */
		table.jqGrid('navGrid', pager,
			{edit:true, add:false, del:false, search: false, refresh:true},
			{
                beforeShowForm		: function(form){
                						$("#editmod"+nameTable).center();
										$('#tr_parametro', form).hide();
										$('#tr_descripcion', form).hide();
										$('#tr_notificacion', form).hide();
									},
				onclickSubmit		: function(rowid){
										var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();										
										return { parametro: $.toJSON(dataForm), sesion: $.getSesionJSON() };
									},
				afterComplete		: function(response){
										try{
											$.setSesion($.evalJSON(response.responseText));
										}catch(e){
											$.obtenJson("sesion/consultar", { sesion: $.evalJSON($.getSesionJSON()) }, function(response){
												$.setSesion(response);
											});
										}
									}
			}
		);
	}

	return {
		init: init
	};
});