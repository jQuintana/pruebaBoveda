/**
 * Módulo JavaScript, con las funcionalidades de la pantalla de la bitacora del sistema
 *
 * @author  	Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Bitacora
 */
 define([],function(){

	/**
	 * Inicializa los eventos del grid de bitacora
	 */
	function init(){
		/**
		 * Inicializamos las variables para el manejo del grid y su paginador
		 */
		var nameTable 	= "table_jsonBitacora";
		var table 		= $("#"+nameTable);
		var pager		= "#div_pager";

		/**
		 * Crea y Configura el grid de la bitacora
		 */
		var mygrid = table.jqGrid({
			url: $.urlBase+'bitacora/listar',
			datatype: "json",
			colNames:['ID','Evento', 'Mensaje', 'Hora','Usuario'],
			colModel:[
				{name:'idBitacora',index:'idBitacora', width:10},
				{name:'evento',index:'evento', width:20},
				{name:'mensaje',index:'mensaje'},
				{name:'hora',index:'hora', width:25, searchoptions:{sopt:['eq','ne'], dataInit:function(elem){$(elem).datepicker({onSelect: function(){if(this.id.substr(0, 3) === "gs_"){setTimeout(function(){mygrid[0].triggerToolbar();}, 50);}else $(this).trigger('change');}});}}},
				{name:'usuario',index:'usuario', hidden: true}
			],
			sortorder: "desc",
			pager: pager,
			sortname: 'idBitacora',
			caption: "Bitac Boveda"
		});

		/**
		 * Configura los enventos deltro del paginador del grid
		 */
		table.jqGrid('navGrid',pager,
			{ edit:false, add:false, del:false, search: false, refresh:true, view:true },
			{},{},{},{},
			{
				beforeShowForm: function(form){
					$("#viewmod"+nameTable).center();				 
                }
			}
		);
		
		/**
		 * Configuracion del paginador, se agregan el boton que permiten manipular el searchtoolbar
		 */
		table.jqGrid('navButtonAdd', pager,{caption:"",title:"Ocultar/Mostrar barra de búsqueda", buttonicon :'ui-icon-pin-s',
			onClickButton:function(){
				mygrid[0].toggleToolbar()
			}
		});

		/**
		 * Configuracion del paginador, se agregan el boton que permiten manipular el searchtoolbar
		 */
		table.jqGrid('navButtonAdd', pager,{caption:"",title:"Limpiar Búsqueda",buttonicon :'ui-icon-refresh',
			onClickButton:function(){
				mygrid[0].clearToolbar()
			}
		});
		
		/**
		 * Configuracion del filterToolbar para el grid
		 */
		table.jqGrid('filterToolbar');
	}

	return {
		init: init
	};
});
