/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de usuario
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Usuario
 */
define([],function(){
	/**
	 * Esta funcion formatea dados los varoles enviados desde el grid para mostrar un icono para el control del
	 * catalogo de roles/usuario
	 */
	function rolFormatter (cellvalue, options, rowObject){
		return '<a href="#userHasRol_'+cellvalue+'" id="'+cellvalue+'"><span class="icon-user"/></a>';
	}
	
	function empresaFormatter (cellvalue, options, rowObject){
		return '<a href="#userHasEmpresa_'+cellvalue+'" id="'+cellvalue+'"><span class="icon-globe"/></a>';
	}

	/**
	 * Formatea la informacion para mostrar el estatus del usuario
	 */
	function estatusFormatter (cellvalue, options, rowObject){
		return (cellvalue == 1) ? "Activo" : "Inactivo";
	}

	/**
	 * Configuramos e inicializamos los eventos que controlan el grid de usuarios
	 */
	function init(){
		/**
		 * inicializamos la bariables que manejaran el grid
		 */
		var nameTable 	= "table_jsonUsuarios";
		var table 		= $("#"+nameTable);
		var pager		= "#div_pager";

		/**
		 * Crea un evento que lanzara un nuevo html para mostrar la tabla de roles de usuario al seleccionar los links
		 * de roles de cada usuario
		 */
		table.delegate("a","click",function(){
			var id = $(this).attr("id");
			var href = $(this).attr('href');
			
			if (href.indexOf('#userHasRol_') === 0) {
				require(['req/text!html/catalogoUsuarioHasRol.html', 'script/catalogoUsuarioHasRol'], function (catalogoUsuarioHasRol_html, catalogoUsuarioHasRol_js) {					
					$(catalogoUsuarioHasRol_html).dialog({						
						autoOpen: true,
						resizable: true,
						title: "Usuario "+id+" Roles",
						modal: true,
						width: "auto",
						height: "auto",
						show: { effect: "fade", duration: 500 },
						hide: { effect: "fade", duration: 500 },
						buttons: {
							Ok: function () {								
								$(this).dialog("close");								
							}
						},
						close: function (event, ui) { $(this).remove(); },						
						open: function (event, ui) { $(".ui-widget-overlay").css({ "z-index": 1030 }); }						
					});
					catalogoUsuarioHasRol_js.init(id);
				});
			} else {
				require(['req/text!html/catalogoUsuarioHasEmpresa.html', 'script/catalogoUsuarioHasEmpresa'], function (catalogoUsuarioHasEmpresa_html, catalogoUsuarioHasEmpresa_js) {					
					$(catalogoUsuarioHasEmpresa_html).dialog({						
						autoOpen: true,
						resizable: true,
						title: "Usuario "+id+" Empresas",
						modal: true,
						show: { effect: "fade", duration: 500 },
						hide: { effect: "fade", duration: 500 },
						position: ['center', 90],
						height: 500,
						width: 410,
						buttons: {
							Ok: function () {								
								$(this).dialog("close");								
							}
						},
						close: function (event, ui) { $(this).remove(); },						
						open: function (event, ui) { $(".ui-widget-overlay").css({ "z-index": 1030 }); }						
					});
					catalogoUsuarioHasEmpresa_js.init(id);
				});
			}			
		});

		/**
		 * Crea y configura el grid de usuario
		 */
		table.jqGrid({
			url: $.urlBase+'usuario/listar',
			datatype: "json",
			colNames:['Usuario', 'Password','Nombre','Apellido Paterno','Apellido Materno','Correo Electrónico','Estatus', 'Roles', 'Empresas'],
			colModel:[
				{ name: 'usuario', index: 'usuario', width: 60, key: true, editable: true, editoptions: { dataInit: function (elem) { $(elem).css('width', "80%"); } }, editrules: { required: true } },
				{ name: 'password', index: 'password', hidden: true, editable: true },				
				{ name: 'nombre', index: 'nombre', width: 150, editable: true, editoptions: { dataInit: function (elem) { $(elem).css('width', "80%"); } }, editrules: { required: true } },				
				{ name: 'apellidoPaterno', index: 'apellidoPaterno', width: 150, editable: true, editoptions: { dataInit: function (elem) { $(elem).css('width', "80%"); } }, editrules: { required: true } },				
				{ name: 'apellidoMaterno', index: 'apellidoMaterno', width: 150, editable: true, editoptions: { dataInit: function (elem) { $(elem).css('width', "80%"); } } },				
				{ name: 'correoElectronico', index: 'correoElectronico', width: 200, editable: true, editoptions: { dataInit: function (elem) { $(elem).css('width', "80%"); } }, editrules: { required: true, email: true } },				
				{ name: 'estatus', index: 'estatus', formatter: estatusFormatter, width: 100, align: "center", editable: true, edittype: "select", editoptions: { value: "1:Activo; 0:Inactivo", dataInit: function (elem) { $(elem).css('width', "80%"); } }, editrules: { required: true } },				
				{ name: 'usuario', index: 'usuario', formatter: rolFormatter, width: 65, align: "center" },
				{ name: 'usuario', index: 'usuario', formatter: empresaFormatter, width: 65, align: "center" }			
			],
			pager: pager,		
			sortname: 'usuario',
			caption: "Usuarios Boveda"
		});

		/**
		 * Configura los eventos en el paginador del grid, la edicion, insercion y eliminacion de registros
		 */
		table.jqGrid('navGrid', pager,
			{edit:true, add:true, del:true, search: false, refresh:true },
			{
				url: $.urlBase+"usuario/actualizar",
                beforeShowForm: function(form){
                	$('#editmod'+nameTable).center();
                    $('#tr_usuario', form).hide();
                },
                onclickSubmit: function(rowid){
					var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
					dataForm.password = null;
					delete dataForm.password;
					return { usuario: $.toJSON(dataForm), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
            },
			{
				url: $.urlBase+"usuario/insertar",
                beforeShowForm: function(form) {
                	$('#editmod'+nameTable).center();
                    var nameColumnField = $('#tr_usuario', form).show();
                    $('<tr class="FormData" id="tr_AddInfo"><td class="CaptionTD ui-widget-content"><b>Información adicional:</b></td></tr>').insertAfter(nameColumnField);
                },
                onclickSubmit:function(rowid){
					var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
					dataForm.password = dataForm.usuario;
					return { usuario: $.toJSON(dataForm), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			},
			{
                url: $.urlBase+"usuario/eliminar",
                beforeShowForm: function(form) {
                	$('#delmod'+nameTable).center();
                },
				onclickSubmit: function(options, rowid){
					return { usuario: $.toJSON({ usuario: rowid }), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			}
		);
	}

	return {
		init: init
	};
});