1.1 (2014-08-27)

	- Fix carga asincrona de scripts con requireJS para ambientes productivos
	- Fix carga de archivos mediante iframe
	- Add manejador de excepciones con DING
	- Add transaccionalidad con AOP
	- Change inyeccion de dependecias mediante annotaciones con DING
	- Change peticiones asincronas desde front-end al back-end
	- Change Model Trait
	- Change Dao Trait
	- Change Service Trait
	- Add Controller Trait
	- Add Aspect Trait
	- Add Dao Interface
	- Add bloqueo de elementos mientras se muestra un dialogo
	- Add se centran elementos desplegados de los Grids
	- Change Handler error para los Grids
	- Add # de intentos para conectar el WS configurable
	- Fix eliminar carpeta creada desde el Zip

1.0 (2014-01-14)

	- Initial Release!